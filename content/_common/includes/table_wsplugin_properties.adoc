
[width=100%, cols="40%,42%,18%", options="header", frame="none", grid= "rows"]
|===
|*Configuration Property* |*Description and Usage* | *Default*

a|`wsplugin.mtom.enabled`
a| _When TRUE enables the support for MTOM._
a|`FALSE`

a|`wsplugin.schema.validation.enabled`
a|_Enable the schema validation.
By default, the schema validation has been disabled due to
performance reasons. For large files, it is recommended to keep the
schema validation as disabled._
a|`FALSE`

a|`wsplugin.messages.pending.list.max`
a|_The maximum number of pending
messages to be listed from the pending messages table. Setting this
property is expected to avoid timeouts due to huge _resultsets_ being
served. Setting this property to zero returns all pending messages._
a|`500`

a|`wsplugin.messages.notifications`
a|The notifications sent by Domibus to the plugin. The following values
are possible:

* `MESSAGE_RECEIVED`
* `MESSAGE_FRAGMENT_RECEIVED`
* `MESSAGE_SEND_FAILURE`
* `MESSAGE_FRAGMENT_SEND_FAILURE`
* `MESSAGE_RECEIVED_FAILURE`
* `MESSAGE_FRAGMENT_RECEIVED_FAILURE`
* `MESSAGE_SEND_SUCCESS`
* `MESSAGE_FRAGMENT_SEND_SUCCESS`
* `MESSAGE_STATUS_CHANGE`
* `MESSAGE_FRAGMENT_STATUS_CHANGE`

a|`MESSAGE_RECEIVED,
MESSAGE_SEND_FAILURE,
MESSAGE_RECEIVED_FAILURE,
MESSAGE_SEND_SUCCESS,
MESSAGE_STATUS_CHANGE`

a|`wsplugin.dispatcher.connectionTimeout`
a|_Timeout values for communication between the ws plugin and the backend service +
ConnectionTimeOut - Specifies the amount of time, in milliseconds, that
the consumer will attempt to establish a connection before it times out.
0 is infinite._
a|`240000`

a|`wsplugin.dispatcher.receiveTimeout`
a|_ReceiveTimeout - Specifies the amount of time,
in milliseconds, that the consumer will wait for a
response before it times out. 0 is infinite._
a|`240000`

a|`wsplugin.dispatcher.allowChunking`
a|_Allows chunking when sending messages to the backend service_
a|`FALSE`

a|`wsplugin.dispatcher.chunkingThreshold`
a|If `domibus.dispatcher.allowChunking` is TRUE, this property sets the
threshold at which messages start getting chunked(in bytes).
Messages under this limit do not get chunked. Defaults to 100 MB.
a|`104857600`

a|`wsplugin.dispatcher.connection.keepAlive`
a|Specifies if the connection will be kept alive between C2-C1 and C3-C4.
a|`TRUE`

a|`wsplugin.dispatcher.worker.cronExpression`
a|_Specify concurrency limits via a "lower-upper" String, e.g. "5-10", or a simple
upper limit String, e.g. "10" (the lower limit will be 1 in this case) +
when sending files,_
a|`0 0/1 * * * ?`

a|`wsplugin.push.enabled`
a|_Enables push notifications to the Backend. +
Properties `wsplugin.push.rules.X`, +
`wsplugin.push.rules.X.recipient`, +
`wsplugin.push.rules.X.endpoint`, +
`wsplugin.push.rules.X.retry` and
`wsplugin.push.rules.X.type` needed +
with X `finalRecipient`._
a|`FALSE`

a|`wsplugin.push.rules.X`
a|_Description of the rule X_
a|-

a|`wsplugin.push.rules.X.recipient`
a|_Recipient that will trigger the rule
(ex: urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1). If empty,
the rule is triggered for any recipient._
a|-

a|`wsplugin.push.rules.X.endpoint`
a|_End point used to submit a message to
the backend (ex: http://localhost:8080/backend)_
a|-

a|`wsplugin.push.rules.X.retry`
a|_Cron expression for the retry mechanism to push to backend_
a|-

a|`wsplugin.push.rules.X.type`
a|Type of notifications to be sent to the Backend:

* `RECEIVE_SUCCESS`
* `RECEIVE_FAIL`
* `SEND_SUCCESS`
* `SEND_FAILURE`
* `MESSAGE_STATUS_CHANGE`
* `SUBMIT_MESSAGE`

See <<wsplugin_backendnotifications, Notifications to the Backend>>.

a|-

a|`wsplugin.push.auth.username`
a|_Basic authentication username that will
be added to the http header of push notification requests to C4. If not
specified, no authorization header will be added._
a|-

a|`wsplugin.push.auth.password`
a|_Basic authentication password that will
be added to the http header of push notification requests to C4. If not
specified, no authorization header will be added._
a|-

a|`wsplugin.push.markAsDownloaded`
a|_If TRUE, the `SUBMIT_MESSAGE` notification also pushes the message. If FALSE, the backend will be able to retrieve the same message multiple times and explicitly set the message status to `DOWNLOADED`._
a|`TRUE`
|===
