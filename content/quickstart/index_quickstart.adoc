:imagesdir: content/_common/images/quickstart/

[[quickstart]]
== Quick Start Guide

.Introduction
The eDelivery Access Point (AP) Domibus implements a standardised
message exchange protocol that ensures interoperable, secure and
reliable data exchange.

Domibus is the AS4 Access Point open source project maintained by
the European Commission.

The current release of Domibus supports Tomcat, WebLogic and WildFly and
contains the following archives, where X.Y.Z refers to the version
number release, for example, if the current release is 5.0.1, then X.Y.Z stands for 5.0.1):

include::../_common/includes/table_binaries_description.adoc[]

.Purpose of this guide
This release contains the AS4 Access Point of the eDelivery building
block.
For more information about this release, please refer to
https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus[Domibus at the eDelivery Portal].

// We should always refer to a specific release in the text even if the doc's
// file naming and cover page states it.

This release of the eDelivery Access Point is the result of significant
collaboration among different EU policy projects, IT delivery teams and
the eDelivery building block. Nevertheless, this eDelivery release is
fully reusable by any other policy domain of the EU.

This release supports:

.Servers
* Tomcat 9.x
* WebLogic Version 12.2.1.4 (tested versions, future versions might also
work)
* WildFly 26.1.x (tested versions, future versions might also work)

.Databases
* Oracle 12c R2 and Oracle 19c
* MySQL 8

In this guide, we are covering Static discovery on Single server
Tomcat/MySQL configuration.

NOTE: For other scenarios such as Dynamic Discovery, Installation on
WildFly or WebLogic please refer to the full <<adminguide, Administration Guide>>
of your corresponding Domibus release.

We will guide you to setup two Tomcat standalone Access Points, deployed
on different machines, to exchange B2B documents securely over AS4 by:

* Deploying and configuring both Access Points (blue and red)
* Configuring processing mode files for both AS4 Access Points
* Using the provided AS4 Access Points certificates
* Setup the Access Points blue and red for running test cases (see $10-
Testing)

.Installation on two different machines
image:image3.png[width=506,height=314]

[NOTE]
====
* The same procedure can be extended to a third (or more) Access Point.
* This guide does not cover the preliminary network configuration allowing communication between separate networks (e.g.: Proxy setup).
====

=== Prerequisite

* Oracle Java runtime environment (JRE) *or* Oracle OpenJDK11:
** Oracle 8u291+ for Tomcat, WildFly and WebLogic. +
http://www.oracle.com/technetwork/java/javase/downloads/index.html

** Oracle OpenJDK 11 version 11.0.11 for Tomcat and WildFly: +
https://openjdk.java.net/projects/jdk/11/

* Database Management Systems :
** MySQL 8
*** Version tested, future versions might work

Please install the above software on your host machine. For further
information and installation details, refer to the manufacturers'
websites.

=== Configure your environment

==== Package Overview

===== Domibus-msh-distribution-X.Y.Z-tomcat-full.zip

Download the Domibus Tomcat Full Distribution from Digital website as
shown in below picture:

`https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus`

.Download the package
image:image4.png[width=315,height=428]

This downloaded package has the following structure:

.Package Contents
image:image5.png[width=248,height=189]

* `<edelivery_path>` contents and structure is as can be seen in the _Package Contents_ figure above, and is not to be confused with the `domibus/conf/domibus` folder subfolder.

* `<edelivery_path>/bin` contains either the _executable batch file_ (Windows) or the _shell script_ (Linux) which are required for launching the Access Point.

* `sql-scripts` contains the required application SQL code that needs to be executed on the MySQL database (and scripts for Oracle DB).

.Contents of the `<edelivery_path>`:
image:image6.png[width=317,height=276]

* `/conf`: where you can find XML the configuration files used to administer your Tomcat and the default domibus configuration file.

* `/logs`: where the logs are stored

* `/webapps`: where the WAR files are stored

* `cef_edelivery_path_/conf/domibus` contains the domibus configuration files:

.<edelivery_path> subdirectories content
image:image7.png[width=328,height=273]

===== Sample Configuration and Resting Resources

*To download:*

* the Domibus sample configuration
* `testing zip`
* `domibus-msh-distribution-X..Z-sample-configuration-and-testing.zip`

☞ Click https://ec.europa.eu/digital-building-blocks/artifact/repository/eDelivery/eu/domibus/domibus-msh-distribution/5.1/domibus-msh-distribution-5.1-sample-configuration-and-testing.zip[here],

*or*

☞ Navigate to from the https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus[eDelivery Portal] and click on *Domibus Sample Configuration and Testing*.


.Download Domibus configuration files
image:image8.png[width=274,height=340]

The Sample Configuration archive has the following
contents and structure:
//and contains pre-configured files for Domibus:

.Pre-configured files for Domibus
image:image9.png[width=477,height=188]

* `<edelivery_path>/test` contains a SOAP UI test project.

* `<edelivery_path>/conf/pmodes` contains two AS4 processing modes XML files (one for blue and other for red Access Point) pre-configured to use compression, payload encryption, message signing and non-repudiation, according to the https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/eDelivery+AS4[eDelivery AS4 profile].
* `<edelivery_path>/conf/domibus/keystores` contains a keystore (with the private keys of Access Point blue and Access Point red) and a
truststore (with the public keys of Access Point _blue_ and Access Point _red_) that can be used by both Access Points. Note that the keystore
contains the private keys of both Access Points blue and red. This setup is not secure and is used for demonstration purpose only. In production,
the private key should only be known, and deployed in the keystore of its owner (one participant). For this test release, each Access Point uses self-signed certificates.

[NOTE.seealso,caption=See Also]
====
For more information about AS4 security, see <<as4security_intro>>.
====

NOTE: The `/conf` folder in the sample archive should be unzipped and *merged*
with the `edelivery_path/conf` folder that already exists.

==== Tomcat Standalone Access Point

As described in the purpose of this guide, we need to configure two
Access Points running on two separate machines. Therefore, the procedure below would need to be applied on both machines:

* Hostname "blue" (<blue_hostname>:8080) and
* Hostname "red" (<red_hostname>:8080).

For this step, you will have to use the following resources:

* *domibus-msh-distribution-X.Y.Z-tomcat-full.zip*

NOTE: All binaries can be downloaded from https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus[Domibus release page in eDelivery Portal]

===== Unzip the archive

Unzip `domibus-msh-distribution-X.Y.Z-tomcat-full.zip` to a location on your
physical machine: `<edelivery_path>` will contain the contents of the Domibus folder as shown below.

.Unzip Domibus configuration files
image:image6.png[width=317,height=276]

===== Unzip the archive domibus-msh-distribution-x.y.z-sample-configuration-and-testing.zip

The *conf* folder in the sample archive should be unzipped and merged with the `<edelivery_path>/conf` folder that already exists. Please ensure that the `<edelivery_path>/conf/domibus` folder structure looks like in the below figure:

.Domibus folder structure
image:image10.png[width=466,height=442]

===== Prepare the MySQL database

You will need admin rights to perform some of these operations):

1. Open a command prompt and navigate to this directory: `sql-scripts`.
2. Execute the following MySQL commands at the command prompt:
+
Please ensure to replace the `root_user` and `root_password` with the corresponding root user and password for MySQL.
+
[source, roomsql]
----
mysql -h localhost -u root_user --password=root_password -e
"drop schema if exists _domibus_;
    create schema domibus;
    alter database domibus charset=utf8mb4 collate=utf8mb4_bin;
    createuser edelivery@localhost identified by 'edelivery';
    grant all on domibus.* to edelivery@localhost;"
----
+
The script above creates a schema (`domibus`) and a user (`edelivery`) that have all the privileges on the schema.
+
NOTE: It is possible that you are now allowed to choose `eDelivery` as the password based on your MySql Password policy. +
In that case, you would need to increase the complexity of your password. +
Please update the new password in Domibus property file accordingly.

3. Execute the following MYSQL commands individually and sequentially:
+
.3.1
[source, roomsql]
----
mysql -h localhost -u root_user --password=root_password -e
"grant xa_recover_admin on *.* to edelivery@localhost;"
----
+
.3.2
[source, roomsql]
----
mysql -h localhost -u root_user --password=root_password
domibus < mysqlinnoDb-x.y.z.ddl
----
+
.3.3
[source, roomsql]
----
mysql -h localhost -u root_user --password=root_password
domibus < mysqlinnoDb-x.y.z-data.ddl
----

4. If you are using MySQL 8 under Windows, then please set the database
timezone. It is recommended that the database timezone is the same as
the timezone of the machine where Domibus is installed.
+
`default-time-zone='+00:00'`
+
[IMPORTANT]
====
*If you are using Windows:*

1. Make sure the parent directory of mysql.exe is added to your PATH.
2. You can also use MYSQL Workbench, instead of the command line statements to create the database.
3. Verify that the`<edelivery_path>/conf/domibus/domibus.properties` file has the relevant database parameters, if required (in case you have changed the username/password or schema name).
====
+
.Database properties
[source, properties]
----
# --------------- Database--------
# Database server name
domibus.database.serverName=localhost

# Database port
domibus.database.port=3306
domibus.database.schema=domibus


# General schema. If uncommented Domibus will run in multi-tenancy mode
#domibus.database.general.schema=general_schema

#Database schema
#Comment this property when Domibus is in multi-tenancy mode.
#Comment this property when Domibus is configured in single tenancy mode with an Oracle database.
domibus.database.schema=domibus

# Non-XA Datasource
# MySQL
# Connector/J 8.0.x
*_domibus.datasource.driverClassName=com.mysql.cj.jdbc.Driver_*
*_domibus.datasource.url=jdbc:mysql://$\{domibus.database.serverName}:$\{domibus.database.port}/$\{domibus.database.schema}?useSSL=false&allowPublicKeyRetrieval=true&useLegacyDatetimeCode=false&serverTimezone=UTC_*
----


5. Please download the `<Mysql V8 Connector Jar file>` from the MYSQL
website and add it to the cef_edelivery/lib folder of the installation:

`cef_edelivery_path\lib\<Mysql V8 Connector Jar file>`

=== Keystore

In order to exchange B2B messages and documents between Access Points
*blue* and *red*, it is necessary to check the following:

[width="100%",cols="50%,50%",options="header"]
|===
|For blue |For red
a|
In `domibus.properties`: the keystore alias property, +
`domibus.security.key.private.alias=blue_gw`

a|
In `domibus.properties`: the keystore alias property, +
`domibus.security.key.private.alias=red_gw`

|===

In a production environment, each participant would need a certificate
delivered by a certification authority and remote exchanges between
business partners would be managed by each partner's PMode (that should
be uploaded on each Access Point).

=== Domibus Config location

Domibus expects a single environment variable `domibus.config.location`,
pointing towards the `<edelivery_path>/conf/domibus` folder.

You can do this by editing the first command lines of:

* `<edelivery_path>\domibus\bin\setenv.bat` (Windows) or
* `<edelivery_path>/bin/setenv.sh` (Linux).

Set `CATALINA_HOME` with the absolute path of the installation `<edelivery_path>/domibus.*`.

*Windows*:

* Edit `<edelivery_path>\domibus\bin\setenv.bat` and add the following:
+
[source, bash]
----
set CATALINA_HOME=<edelivery_path>
set CATALINA_TMPDIR=<path to _tmp directory>
set JAVA_OPTS=%JAVA_OPTS% -Dfile.encoding=UTF-8 -Xms128m -Xmx1024m
 -XX:PermSize=64m
set JAVA_OPTS=%JAVA_OPTS%
-Ddomibus.config.location=%CATALINA_HOME%\conf\domibus*
----

*Linux*:

1. Edit `<edelivery_path>/bin/setenv.sh` +
by adding the following:

[source,bash]
----
export CATALINA_HOME=<edelivery_path>
export CATALINA_TMPDIR=<path to _tmp directory>
export JAVA_OPTS="$JAVA_OPTS -Xms128m -Xmx1024m"
export JAVA_OPTS="$JAVA_OPTS
-Ddomibus.config.location=$*_CATALINA_HOME/conf/domibus"
----

=== Launch the Domibus application

*Windows:* +
Execute,

1. `cd <edelivery_path>\bin\`
2. `startup.bat`

*Linux:*

1. Execute `cd <edelivery_path>/bin/chmod u+x *.sh ./startup.sh`

2. Nav to: http://localhost:8080/domibus/home[http://localhost:8080/domibus] to display the Domibus home page on your browser. +
If you can access the page, it means the deployment was successful.
+
NOTE: By default user=admin. For the password, look in the logs for the phrase: +
“Default password for user admin is”. +
You will be asked to change the default password when logging in for
the first time.
+
.Domibus administration page
image:image11.jpg[width=604,height=291]
+
a. To allow the remote application to send a message to this machine, you
would need to create a dedicated rule (to allow this port) from your
local firewall. +
*See also* ☞ <<firewallsettings>>
b. If you intend to install both Access Points on the same server, you will need to change the ports of the red Access Point and create a separate
database schema, update the domibus.properties file and change the ActiveMQ ports before starting the server to avoid conflicts.

=== Upload PModes

Edit the two PMode files:

* `<edelivery_path>/conf/pmodes/domibus-gw-sample-pmode-blue.xml`
* `domibus-gw-sample-pmode-red.xml`

and replace `<blue_hostname>` and `<red_hostname>` with their real hostnames or IPs:

.PMode view
[source,xml]
----
<party name="red_gw" endpoint="Error! Hyperlink reference not valid.">
    <identifier partyId="domibus-red" partyIdType="partyTypeUrn" />
</party>
<party name="blue_gw" endpoint="Error! Hyperlink reference not valid.">
    <identifier partyId="domibus-blue" partyIdType="partyTypeUrn" />
</party>
----

[NOTE.seealso,caption=See Also]
====
For more details about the provided PMode, see <<processingmode>>.
====

=== Upload the PMode file on both Access Points

To upload a PMode XML file,

1. Connect to the administration console using your credentials.
+
NOTE: By default `login=admin`. For the password,
look in the logs for the phrase: `Default password for user admin is`)
to `http://localhost:8080/domibus`:
+
.Login to the administration console
image:image11.jpg[width=604,height=291]

2. Select the menu *PMode* -> *Current* -> *Upload*.
+
.PMode update
image:image12.jpg[width=527,height=423]
+
A popup window appears where you can select the PMode file.

3. Select PMode file and click on the *Upload* button.

When the operation is successful you will get the following window:

.PMode upload success
image:image13.png[width=642,height=289]

Now your Tomcat Access Points are running and ready to send or receive messages.

NOTE: Every time a PMode is updated, the Truststore is also refreshed from the file system.

=== Test

As explained in the Release Notes document, and to facilitate testing,
we have developed a Reference Web Service endpoint to illustrate how
participants can connect and interact with the AS4 Access Point to send
messages.

In addition, it is possible for the backends to download received
messages from their Access Point using a request (`downloadMessage`)
defined in the same WSDL.

[NOTE.seealso,caption=See Also]
====
For more details about:

* The interface control description for the WS default plugin, see <<wsplugin_interface, WS Plugin Interface>>
* Testing with a SoapUI Project, see the <<domibus_testing, Testing Guide>>
* WSD, see the https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus+releases[Domibus Releases page]
====

NOTE: Domibus provides three default plugins for sending and
receiving/downloading messages via Domibus, a Web Service plugin, a JMS
plugin and a File System plugin. The Web Service plugin is deployed by
default with the tomcat-full distribution. For more information about
the Other Plugins please refer to the complete Domibus admin guide.

[[qsg_parameters]]
=== Annex 1 - Parameters

[caption=, title=Local and Remote Access Points Parameters]
[width="100%",cols="24%,38%,38%",options="header",]
|===
a|Parameters
a|Local Access Point +
(Gateway "blue")
a|Remote Access Point +
(Gateway "red")

a|*Hostname*
a|`<blue_hostname>:8080`
a|`<red_hostname>:8080`

a|*Database*
a|MySQL database
a|MySQL database

a|*Administrator Page*
a|Username: `admin`. For the password, look in the logs for the phrase: “Default password for user admin is”. +
http://localhost:8080/domibus/home
a|Username: `admin`. For the password, look in the logs for the phrase: “Default password for user admin is”. +
http://localhost:8180/domibus/home[http://localhost:8080/domibus/home]

a|*Database Schema*
a|edelivery
a|edelivery

a|*Database connector*
a|Username: `edelivery` +
Password: `edelivery` +
`jdbc:mysql://localhost:3306/domibus`
a|Username: `edelivery` +
Password: `edelivery` +
`jdbc:mysql://localhost:3306/domibus`

a|*DB username/password*
a|edelivery/edelivery
a|edelivery/edelivery

a|*PModes XML files*
a|`pmodes/domibus-gw-sample-pmode-blue.xml`
a|`pmodes/domibus-gw-sample-pmode-red.xml`
|===

NOTE: _localhost_ represents the server name that hosts the database and the
application server for their respective Access Point.

[[firewallsettings]]
=== Annex 2 - Firewall Settings

The firewall settings may prevent you from exchanging messages between
your local and remote Tomcat Access Points.

To test the status of a port, run the command:

`telnet <server_ip> <port>`

Tomcat uses the following ports, make sure those are opened on both
machines "blue" and "red" (TCP protocol):

* `8080` (HTTP port)
* `3306` (MySQL port)

This is how you can open a port on the Windows Firewall:

1. Click on *Start* -> *Control Panel*
2. Go to *Windows* -> *Firewall* and click on *Advanced Settings*
3. Right-click on *Inbound Rules* and select *New Rule*:
+
image:image14.png[width=366,height=377]

4. Select *Port* and click on *Next*:
+
image:image15.png[width=376,height=303]

5. Enter a specific local port (e.g. 8080) and click on *Next*:
+
image:image16.png[width=378,height=303]

6. Click on *Next*:
+
image:image17.png[width=394,height=317]

7. Choose a name for the new rule and click on *Finish* to end:
+
image:image18.png[width=407,height=328]

[[processingmode]]
=== Annex 3 - Processing Mode

Processing modes (PModes) describe how messages are exchanged between
AS4 partners (Access Point blue and Access Point red). These files
contain the identifiers of each AS4 Access Point (identified as parties
in the PMode file below).

Sender Identifier and Receiver Identifier represent the organizations
that send and receive the business documents (respectively "domibus-
blue" and "domibus-red"). They are both used in the authorization
process (PMode). Therefore, adding, modifying or deleting a participant
implies modifying the corresponding PMode files.

Here is an example of the content of a PMode XML file:

* In this setup we have allowed each party (blue_gw or red_gw) to
initiate the process. If only blue_gw is supposed to send messages, we
need to put only blue_gw in <initiatorParties> and red_gw in
<responderParties>.

[source, xml]
----
<?xml version="1.0" encoding="UTF-8"?>
<db:configuration
    xmlns:db="http://domibus.eu/configuration" party="blue_gw">
    <mpcs>
        <mpc name="defaultMpc"
            qualifiedName="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC"
            enabled="true"
            default="true"
            retention_downloaded="0"
            retention_undownloaded="14400"/>
    </mpcs>
    <businessProcesses>
        <roles>
            <role name="defaultInitiatorRole" value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator"/>
            <role name="defaultResponderRole" value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder"/>
        </roles>
        <parties>
            <partyIdTypes>
                <partyIdType name="partyTypeUrn" value="urn:oasis:names:tc:ebcore:partyid-type:unregistered"/>
            </partyIdTypes>
            <party name="red_gw"  endpoint="http://
                <red_hostname>:8080/domibus/services/msh">
                    <identifier partyId="domibus-red" partyIdType="partyTypeUrn"/>
                </party>
                <party name="blue_gw" endpoint="http://
                    <blue_hostname>:8080/domibus/services/msh">
                        <identifier partyId="domibus-blue" partyIdType="partyTypeUrn"/>
                    </party>
                </parties>
                <meps>
                    <mep name="oneway" value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/oneWay"/>
                    <mep name="twoway" value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/twoWay"/>
                    <binding name="push" value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/push"/>
                    <binding name="pushAndPush" value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/push-and-push"/>
                </meps>
                <properties>
                    <property name="originalSenderProperty"
                            key="originalSender"
                            datatype="string"
                            required="true"/>
                    <property name="finalRecipientProperty"
                            key="finalRecipient"
                            datatype="string"
                            required="true"/>
                    <propertySet name="eDeliveryPropertySet">
                        <propertyRef property="finalRecipientProperty"/>
                        <propertyRef property="originalSenderProperty"/>
                    </propertySet>
                </properties>
                <payloadProfiles>
                    <payload name="businessContentPayload"
                            cid="cid:message"
                            required="true"
                            mimeType="text/xml"/>
                    <payload name="businessContentAttachment"
                            cid="cid:attachment"
                            required="false"
                            mimeType="application/octet-stream"/>
                    <payloadProfile name="MessageProfile" maxSize="40894464">
                        <attachment name="businessContentPayload"/>
                        <attachment name="businessContentAttachment"/>
                    </payloadProfile>
                </payloadProfiles>
                <securities>
                    <security name="eDeliveryAS4Policy"/>
                </securities>
                <errorHandlings>
                    <errorHandling name="demoErrorHandling"
                                errorAsResponse="true"
                                businessErrorNotifyProducer="true"
                                businessErrorNotifyConsumer="true"
                                deliveryFailureNotifyProducer="true"/>
                </errorHandlings>
                <agreements>
                    <agreement name="agreement1" value="A1" type="T1"/>
                </agreements>
                <services>
                    <service name="testService1" value="bdx:noprocess" type="tc1"/>
                    <service name="testService" value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/service"/>
                </services>
                <actions>
                    <action name="tc1Action" value="TC1Leg1"/>
                    <action name="testAction" value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/test"/>
                </actions>
                <as4>
                    <receptionAwareness name="receptionAwareness"
                                        retry="12;4;CONSTANT"
                                        duplicateDetection="true"/>
                    <reliability name="AS4Reliability"
                                nonRepudiation="true"
                                replyPattern="response"/>
                </as4>
                <legConfigurations>
                    <legConfiguration name="pushTestcase1tc1Action"
                                        service="testService1"
                                        action="tc1Action"
                                        defaultMpc="defaultMpc"
                                        reliability="AS4Reliability"
                                        security="eDeliveryAS4Policy"
                                        receptionAwareness="receptionAwareness"
                                        propertySet="eDeliveryPropertySet"
                                        payloadProfile="MessageProfile"
                                        errorHandling="demoErrorHandling"
                                        compressPayloads="true"/>
                    <legConfiguration name="testServiceCase"
                                        service="testService"
                                        action="testAction"
                                        defaultMpc="defaultMpc"
                                        reliability="AS4Reliability"
                                        security="eDeliveryAS4Policy"
                                        receptionAwareness="receptionAwareness"
                                        propertySet="eDeliveryPropertySet"
                                        payloadProfile="MessageProfile"
                                        errorHandling="demoErrorHandling"
                                        compressPayloads="true"/>
                </legConfigurations>
                <process name="tc1Process"
                            mep="oneway"
                            binding="push"
                            initiatorRole="defaultInitiatorRole"
                            responderRole="defaultResponderRole">
                    <initiatorParties>
                        <initiatorParty name="blue_gw"/>
                        <initiatorParty name="red_gw"/>
                    </initiatorParties>
                    <responderParties>
                        <responderParty name="blue_gw"/>
                        <responderParty name="red_gw"/>
                    </responderParties>
                    <legs>
                        <leg name="pushTestcase1tc1Action"/>
                        <leg name="testServiceCase"/>
                    </legs>
                </process>
            </businessProcesses>
        </db:configuration>
----

=== Annex 4 - Domibus Pconf to ebMS3 mapping

The following table provides additional information concerning the
Domibus PMode configuration (pconf) files.

[caption=, title=Domibus pconf to ebMS3 mapping]
[width="100%",cols="30%,25%,45%",options="header"]
|===
|Domibus pconf |EbMS3 Specification + [ebMS3CORE] [AS4-Profile]
| Description

a|`MPCs` |- |Container which defines the different MPCs (Message Partition Channels).

a|`MPC`
a|`PMode[1].BusinessInfo.MPC`: +
The value of this parameter is the
identifier of the MPC (Message Partition Channel) to which the message
is assigned. It maps to the attribute `Messaging/UserMessage`
a|Message Partition Channel allows the partition of the flow of messages
from a `Sending MSHs` to a `Receiving MSH` into several flows, each of
which is controlled separately. An MPC also allows merging flows from
several `Sending MSHs` into a unique flow that will be treated as such
by a `Receiving MSH`. +
The value of this parameter is the identifier of the MPC to which the
message is assigned.

a|`MessageRetentionDownloaded` |- |Retention interval for messages already delivered to the backend.

a|`MessageRetentionUnDownloaded` |- |Retention interval for messages not yet delivered to the backend.

a|`Parties` |- |Container which defines the different PartyIdTypes, Party and Endpoint.

a|`PartyIdTypes`
a|maps to the attribute +
`Messaging/UserMessage/PartyInfo`
|Message Unit bundling happens when the Messaging element contains multiple child elements or Units (either User Message Units or Signal Message Units).

a|`Party ID`
a|maps to the element +
`Messaging/UserMessage/PartyInfo`
|The ebCore Party ID type can simply be used as an identifier format and therefore as a convention for values to be used in configuration and – as such – does not require any specific solution building block.

a|`Endpoint`
a|maps to +
`PMode[1].Protocol.Address`
a|The endpoint is a party attribute that contains the link to the MSH.
+
The value of this parameter represents the address (endpoint URL) of the
`Receiver MSH` (or `Receiver Party`) to which Messages under this PMode
leg are to be sent. Note that a URL generally determines the transport
protocol (e.g. if the endpoint is an email address, then the transport
protocol must be SMTP; if the address scheme is "http", then the
transport protocol must be HTTP).

|AS4 |- |Container

a|Reliability [@Nonrepudiation] [@ReplyPattern]
a|
`Nonrepudiation` maps to +
`PMode[1].Security.SendReceipt.NonRepudiation`

`ReplyPattern` maps to +
`PMode[1].Security.SendReceipt.ReplyPattern`

a|`PMode[1].Security.SendReceipt.NonRepudiation`:

* value=‘true' (to be used for non-repudiation of receipt),
* value ='false' (to be used simply for reception awareness).
+
`PMode[1].Security.SendReceipt.ReplyPattern`:
* value = ‘Response’ (sending receipts on the HTTP response or back-channel).
+
`PMode[1].Security.SendReceipt.ReplyPattern`:

* value = ‘Callback’ (sending receipts use a separate connection.)

|`ReceptionAwareness` [@retryTimeout] [@retryCount] [@strategy] [@duplicateDetection]
a|
`retryTimeout` maps to +
`PMode[1].ReceptionAwareness.Retry=true`
+
`retryCount` maps to +
`PMode[1].ReceptionAwareness.Retry.Parameters`

`strategy` maps to +
`PMode[1].ReceptionAwareness.Retry.Parameters`

`duplicateDetection` maps to +
`PMode[1].ReceptionAwareness.DuplicateDetection`
``
a|These parameters are stored in a composite string.

* `retryTimeout` defines timeout in seconds.
* `retryCount` is the total number of retries.
* `strategy` defines the frequency of retries. The only _strategy_
available as of now is `CONSTANT`.
* `duplicateDetection` allows to check duplicates when receiving twice
the same message. The only `duplicateDetection` available as of now is `TRUE`.

|`Securities` |- |Container
|`Security` |- |Container

|`Policy`
|`PMode[1].Security.*` NOT including `PMode[1].Security.X509.Signature.Algorithm`
|The parameter in the pconf file defines the name of a WS-SecurityPolicy file.

|`SignatureMethod`
|`PMode[1].Security.X509.Signature.Algorithm`
|This parameter is not supported by WS-SecurityPolicy and therefore it is defined separately.

|`BusinessProcessConfiguration` |- |Container

|Agreements
a|
maps to +
`eb:Messaging/UserMessage/CollaborationInfo/AgreementRef`

|This optional element occurs zero times or once. The `AgreementRef`
element is a string that identifies the entity or artifact governing the
exchange of messages between the parties.

|Actions |- |Container

|Action
a|
maps to +
`Messaging/UserMessage/CollaborationInfo/Action`

|This *required* element occurs once. The element is a string identifying
an operation or an activity within a Service that may support several of these.

|`Services` |- |Container

|`ServiceTypes` Type
a|
maps to +
`Messaging/UserMessage/CollaborationInfo/Service[@type]`

|This *required* element occurs once. It is a string identifying the
service that acts on the message and it is specified by the designer of
the service.

|`MEP [@Legs]`
|-
|An ebMS MEP defines a typical choreography of ebMS User
Messages which are all related through the use of the referencing
feature (RefToMessageId). Each message of an MEP Access Point refers to
a previous message of the same Access Point, unless it is the first one
to occur. Messages are associated with a label (e.g. `request`, `reply`)
that precisely identifies their direction between the parties involved
and their role in the choreography.

|`Bindings` |- |Container

|`Binding`
|-
|The previous definition of ebMS MEP is quite abstract and
ignores any binding consideration to the transport protocol. This is
intentional, so that application level MEPs can be mapped to ebMS MEPs
independently of the transport protocol to be used.

|`Roles` |- |Container

|`Role`
|maps to `PMode.Initiator.Role or PMode.Responder.Role` depending
on where this is used. In ebMS3 message this defines the content of the
following element:
+
For Initiator: +
`Messaging/UserMessage/PartyInfo/From/Role`

For Responder: +
`Messaging/UserMessage/PartyInfo/To/Role`
a|
The *required* role element occurs once, and identifies the authorized
role (`fromAuthorizedRole` or `toAuthorizedRole`) of the Party sending
the message (when present as a child of the `From` element), or
receiving the message (when present as a child of the `To` element). The
value of the role element is a non-empty string, with a default value of
`http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultRole`.

Other possible values are subject to partner agreement.

a|`Processes` |- |Container

a|`PayloadProfiles` |- |Container

a|`Payloads` |- |Container

a|`Payload`
a|maps to +
`PMode[1].BusinessInfo.PayloadProfile`
a|This parameter allows specifying some constraint or profile on the
payload. It specifies a list of payload parts.

A payload part is a data structure that consists of five properties:

* *name* (or Content-ID) that is the *part identifier*, and can be used
as an index in the notation PayloadProfile;
* *MIME data type* (text/xml, application/pdf, etc.);
* *name of the applicable XML Schema file* if the MIME data type is
text/xml;
* *maximum size in bytes*;
* *boolean* string indicating whether the part is required or optional, within the User message.

The message payload(s) must match this profile.

a|`ErrorHandlings` |- |Container

a|`ErrorHandling` |- |Container

a|`ErrorAsResponse`
a|
maps to +
`PMode[1].ErrorHandling.Report.AsResponse`
|This Boolean parameter indicates (if `true`) that errors generated from
receiving a message in error are sent over the back-channel of the
underlying protocol associated with the message in error. If `false`,
such errors are not sent over the back channel.

a|`ProcessErrorNotifyProducer`
a|
maps to +
`PMode[1].ErrorHandling.Report.ProcessErrorNotifyProducer`
|This Boolean parameter indicates whether (if `true`) the Producer
(application/party) of a User Message matching this PMode should be
notified when an error occurs in the Sending MSH, during processing of
the `User Message to be sent`.

a|`ProcessErrorNotifyConsumer`
a|
maps to +
`PMode[1].ErrorHandling.Report.ProcessErrorNotifyProducer`
|This Boolean parameter indicates whether (if `true`) the Consumer
(application/party) of a User Message matching this PMode should be
notified when an error occurs in the Receiving MSH, during processing of
the `received User message`.

a|`DeliveryFailureNotifyProducer`
a|
maps to +
`PMode[1].ErrorHandling.Report.DeliveryFailuresNotifyProducer`
|When sending a message with this reliability requirement (`Submit`
invocation), one of the two following outcomes shall occur:

* The Receiving MSH successfully delivers (`Deliver` invocation) the message to the Consumer.
* The Sending MSH notifies (`Notify` invocation) the Producer of a delivery failure.

a|`Legs` |- |Container

a|`Leg`
|-
a|Because messages in the same MEP may be subject to different
requirements - e.g. the reliability, security and error reporting of a
response may not be the same as for a request – the PMode will be
divided into `legs`. Each user message label in an ebMS MEP is
associated with a PMode leg. Each PMode leg has a full set of parameters
for the six categories above (except for `General Parameters`), even
though in many cases parameters will have the same value across the MEP
legs. Signal messages that implement transport channel bindings (such as
PullRequest) are also controlled by the same categories of parameters,
except for `BusinessInfo group`.

a|`Process` |- |In `Process` everything is plugged together.
|===

[[as4security_intro]]
=== Annex 5 - Introduction to AS4 security

To secure the exchanges between Access Points "blue" and "red" (Access
Point "blue" is sending a message to Access Point "red" in this
example), it is necessary to set up each Access Point's keystore and
truststore accordingly. +
The diagram below shows a brief explanation of the main steps of this process:

It is necessary to open the required ports when Access Point blue or
Access Point red is behind a local firewall. For instance, the port 8080
is not opened by default in Windows. Therefore, we would need to create
a dedicated rule on Windows firewall to open the TCP `8080` port.

*See also* <<firewallsettings>>.

