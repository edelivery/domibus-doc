===== Key definitions

Purpose of the File System Plugin::
The purpose of the File System Plugin is to allow the Backends (corner 1
and corner 4) to exchange messages as files simply using the file system
interface without the need of accessing web services.

Access Point::
According to eDelivery, an Access Point is an implementation of the
OpenPEPPOL AS2 Profile or the eDelivery AS4 Profile. The data exchange
protocols of eDelivery are profiles, meaning that several options of the
original technical specifications were narrowed down in order to
increase consistency, interoperability and to simplify deployment. The
profile of AS2 was developed by OpenPEPPOL6, and the profile of AS4 was
developed by e-SENS in collaboration with several service providers
while being implemented in the e-Justice domain by e-CODEX. An Access
Point exposes two interfaces:

* An interface to connect the Backend system with the Access Point.
Typically, this interface is customisable as communication between
Access Points and Backend systems may use any messaging or transport
protocol.
* A standard messaging interface between Access Points, this interface
is configurable according to the options of the profiles supported by
eDelivery. It is important to note that eDelivery standardises the
communication only between the Access Points.

eDelivery AS4 Profile::
The eDelivery AS4 profile is an open technical specification for the
secure and payload-agnostic exchange of data using Web Services.
According to OASIS, the AS4 protocol is the modern successor of the AS2
protocol. AS4 itself is a profile of the ebXML Messaging Services (ebMS)
v.3.0, a broader specification built on top of the SOAP with attachments
specification.
