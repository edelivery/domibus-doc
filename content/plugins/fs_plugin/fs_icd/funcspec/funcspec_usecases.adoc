:imagesdir: images/fsplugin/interface/

[[fsplugin_usecases]]
===== Use case overview

.Actors' Description
[width="100%",cols="24%,76%",options="header"]
|===
|Actor |Description

|FS Plugin Admin |Any participant with administrative rights to
configure the FS Plugin behaviour and access to the backend file system.

|Backend FS1 User |Any participant (human or system) sending file
messages to a recipient Backend C4 and using the Sending AP C2 in that
purpose via the FS Plugin.

|Backend FS2 User |Any participant (human or system) downloading file
messages from any sender Backend C1 and using the Receiving AP C3 in
that purpose via the FS Plugin.
|===

image:image2.png[width=337,height=130]

[[fsplugin_actor]]
.FS Plugin (Actor)
[width="100%",cols="9%,22%,46%,23%",options="header"]
|===
|ID |UC |Short Description |System
|<<uc1,UC01>> | <<uc1,Configure FS Plugin>> |Configure the available properties for the
FS Plugin file system access and behaviour |Backend File System 1 +
Backend File System 2
|===

image:image3.png[width=412,height=391]

[[backendfs1_actor]]
.Backend FS1 (Actor)
[width="100%",cols="9%,22%,46%,23%",options="header"]
|===
|ID |UC |Short Description |System
|<<uc2,UC02>> | <<uc2,Access FS>> |Access or connect to the file system interface (local
or remote). This operation may require authentication if the file system
is secured. For more information see section 3.2 - Supported File
Systems. |Backend File System 1

|<<uc3,UC03>> | <<uc3,Configure Metadata>> |Configure the metadata.xml file in order to
define the file messages submission properties (e.g., From PartyId, To
PartyId). |Backend File System 1

|<<uc4,UC04>> | <<uc4,Send Files >>|Drop the files in the backend file system OUT folder
in order to trigger the sending process according to the existing
metadata. The files are polled by the FS Plugin of C2. |Backend File
System 1

|<<uc5,UC05>> | <<uc5,Check File Status>> |Check the sending process status listing the
file name that will be updated according to the messageId and status.
|Backend File System 1
|===

image:image4.png[width=392,height=226]

[[backendfs2_actor]]
.Backend FS2 (Actor)
[width="100%",cols="9%,22%,46%,23%",options="header"]
|===
|ID |UC |Short Description |System
|<<uc2,UC02>> | <<uc2,Access FS>> |Access or connect to the file system interface (local
or remote). This operation may require authentication. |Backend File
System 2.

|<<uc6,UC06>> | <<uc6,Receive Files>> |Receive the sent files in the configured backend
file system location. These file messages are PUSHED by the FS Plugin
into the receiving backend file system. |Backend File System 2
|===

.Use Cases

====
The Interface Functional Specification is described in the detailed uses
cases using the given examples.

IMPORTANT: Note that the Inputs and Outputs provided as examples for the uses cases are based on a specific PMode configuration.

As defined in the AS4 Profile of ebMS 3.0, a processing mode – or PMode
– is a collection of parameters defining how user messages are exchanged
between a pair of Access Points with respect to Quality of Service,
Transmission Mode and Error Handling. A PMode maps the recipient Access
Point from the partyId, which represents the backend offices associated
to this Access Point.
====

====== UC01 - Configure FS Plugin
.Click to Open
[%collapsible]
====
.UC01 - Configure FS Plugin
[[uc1]]

Brief description:: The FS Plugin Admin configures the plugin behaviour by editing the
fs-plugin.properties file.

Actors:: FS Plugin Admin

Preconditions::

* The FS Plugin must be installed on the Access Point.
* The system user (FS Plugin Admin) as read/write access to the
fs-plugin.properties file.

Basic flow event::

1. The system user (FS Plugin Admin) edits the fs-plugin.properties file.
2. The system user (FS Plugin Admin) updates the existing properties
according to the full specification of configuration options is listed
in <<behavspec_config>> and section 3.2.

Alternative flows::

N/A

Exception flows::

* 2a. The system user (FS Plugin Admin) creates or updates a FS Plugin
property with an invalid value.

* 2a1. The FS Plugin will decay the invalid configuration into the
property default value as listed in <<behavspec_config>>.

Post conditions::

* Successful conditions: The FS Plugin is successfully configured. After
the Access Point restart, it is ready to exchange messages from the file
system interface.

* Failure conditions: The FS Plugin is not correctly configured and is not
ready to properly send and receive files as AS4 messages.

<<fsplugin_usecases, Top>>
<<fsplugin_actor, FS Plugin (Actor)>>
<<backendfs1_actor, Backend FS1 (Actor)>>
<<backendfs2_actor, Backend FS2 (Actor)>>
====

//PDF test, works well
====== UC02 - Access FS

ifdef::backend-pdf[]
[[uc2]]

Brief description::
To use the file system interface, the Backend FS1 User needs to access
the Access Point - FS Plugin via the configured file system folder
within a domain. This use case is realized by the Backend FS1 User by
the navigating into the configured folder (locally or remotely). If the
file system domain location is secured then the user must provide its
credentials to connect.

For detailed information about Domains and Supported File Systems please
see chapters 3.1.4, 3.1.5 and 3.2.

Actors::
Backend FS1 User

Preconditions::

* The Backend FS1 User must have read/write access on its domain folder
and subfolders.

Basic flow event::

1. The Backend FS1 User navigates into the configured outgoing/incoming
folder. This can be done using a graphical explorer or via command line.

Alternative flows::

* 1a. If the folder is remote and secured by username and password then
the credentials will be required and prompted in the first access.

* 1a1. The Backend FS1 User specifies his valid username and password.

Exception flows::

* 1a2. The Backend FS1 User specifies invalid credentials.

Post conditions::

* *Successful conditions*: The Backend FS1 User accesses the contents of the
filesystem folder.

* *Failure conditions*: The Backend FS1 User cannot access the contents of
the filesystem folder.

endif::[]

.Click to Open
[%collapsible]
====
.UC02 - Access FS
[[uc2]]

Brief description::
To use the file system interface, the Backend FS1 User needs to access
the Access Point - FS Plugin via the configured file system folder
within a domain. This use case is realized by the Backend FS1 User by
the navigating into the configured folder (locally or remotely). If the
file system domain location is secured then the user must provide its
credentials to connect.

For detailed information about Domains and Supported File Systems please
see chapters 3.1.4, 3.1.5 and 3.2.

Actors::
Backend FS1 User

Preconditions::

* The Backend FS1 User must have read/write access on its domain folder
and subfolders.

Basic flow event::

1. The Backend FS1 User navigates into the configured outgoing/incoming
folder. This can be done using a graphical explorer or via command line.

Alternative flows::

* 1a. If the folder is remote and secured by username and password then
the credentials will be required and prompted in the first access.

* 1a1. The Backend FS1 User specifies his valid username and password.

Exception flows::

* 1a2. The Backend FS1 User specifies invalid credentials.

Post conditions::

* *Successful conditions*: The Backend FS1 User accesses the contents of the
filesystem folder.

* *Failure conditions*: The Backend FS1 User cannot access the contents of
the filesystem folder.

•
<<fsplugin_usecases, Top>> •
<<fsplugin_actor, FS Plugin (Actor)>> •
<<backendfs1_actor, Backend FS1 (Actor)>> •
<<backendfs2_actor, Backend FS2 (Actor)>>
====

====== UC03 – Configure Metadata
.Click to Open
[%collapsible]
====
.UC03 – Configure Metadata
[[uc3]]

Brief description::

The Backend FS1 User writes the metadata.xml file which contains the
basis of the AS4 metadata that will be used by the FS Plugin to create
the User Message (mapped to the Submission) that will be sent to Access
Point. The metadata.xml file name is reserved by the FS Plugin and it
needs to always be present as a child of the OUT folder or any
sub-directory under the OUT folder.

The metadata file format is detailed in Section 3.3 and is available an
example (example_metadata.xml) in the config/ directory.

Actors::
Backend FS1 User

Preconditions::
* The Backend FS1 User must have read/write access on its domain folder
and subfolders.

Basic flow event::
1. The Backend FS1 User navigates to the OUT folder or any of its
sub-directories.
2. The Backend FS1 User creates the metadata.xml file based on a template
(Cf.: 6.2 - metadata.xml example) in the outgoing folder.
3. The Backend FS1 User edits the metadata.xml file to specify the AS4
metadata, namely the UserMessage, according to the format detailed in
section 3.3.

Alternative flows::
N/A

Exception flows::

* 2a. The Backend FS1 does not create the metadata.xml file in the
outgoing folder.

* 3a. The Backend FS1 creates a metadata.xml file containing an invalid
structure.

Post conditions::

Successful conditions: The metadata.xml is configured and prepared to
define the UserMessage header in the FS Plugin AS4 messages.

Failure conditions: The metadata.xml is not properly configured and is
not prepared to define the UserMessage header in the FS Plugin AS4
messages. Either because it does not exist or has an invalid structure.

•
<<fsplugin_usecases, Top>> •
<<fsplugin_actor, FS Plugin (Actor)>> •
<<backendfs1_actor, Backend FS1 (Actor)>> •
<<backendfs2_actor, Backend FS2 (Actor)>>
====

====== UC04 – Send Files
.Click to Open
[%collapsible]
====
.UC04 – Send Files
[[uc4]]

Brief description::
This Use Case is triggered by the Backend FS1 User, by dropping a
content file into the outgoing folder (C1) of one of the locations
configured in fs-plugin.properties file. It is assumed that valid
metadata has been configured via UC03 beforehand. This Use Case should
result in an AS4 message delivery from C2 to C3.
+
On its own schedule, Domibus core eventually connects to the applicable
destination gateway and delivers the message in conformance to the AS4
protocol.
+
For detailed information about the Send Files sequence see chapter 3.4.

Actors::
Backend FS1 User

Preconditions::
* The FS Plugin is properly configured and activated in the Domibus
Access Point.
* The outgoing folder is prepared to send files as AS4 messages having
the metadata.xml file properly created and configured.

Basic flow event::
1. The Backend FS1 User navigates into the outgoing folder of C1.
2. The Backend FS1 User drops one or more content files into the outgoing
folder.
3. The FS Plugin (C2) continually polls the various configured locations
for a list of existing files in the outgoing folders.
4. The FS Plugin (C2) creates as AS4 message for each content file and
submits to Domibus core. If the content file (i.e file1.doc) is
accompanied by a corresponding .lock file (i.e. file1.doc.lock), the
content file is not touched. Once the content file starts being
processed, the FS Plugin creates the corresponding. lock file.
5. The FS Plugin (C2) renames the submitted content file adding the AS4
message identifier. It also deletes the corresponding. lock file.
6. On its own schedule, Domibus core eventually connects to the
applicable destination gateway and delivers the message in conformance
to the AS4 protocol.

Alternative flows::
* 4a. The FS Plugin (C2) skips to create the AS4 message because
metadata.xml file is missing.

* 4a1. The FS Plugin (C2) logs the skipped files warning that the
metata.xml is missing.

Exception flows::
* 4b. The FS Plugin (C2) fails to create the AS4 message because
metadata.xml file is invalid.

* 4b1. The FS Plugin (C2) logs the schema validation error.

* 4c. The FS Plugin (C2) fails to create the AS4 message because the
content file can’t be accessed.

* 4c1. The FS Plugin (C2) logs the file access error.

* 4c2. The FS Plugin (C2) marks the current message transaction for
rollback (and let Domibus retry in its next polling).

Post conditions::
* *Successful conditions:* The content file is successfully submitted as an
AS4 message and delivered from (C2) to (C3).

* *Failure conditions:* The AS4 message is not successfully created and
can’t be submitted to Domibus core (C2). Error details are logged.

•
<<fsplugin_usecases, Top>> •
<<fsplugin_actor, FS Plugin (Actor)>> •
<<backendfs1_actor, Backend FS1 (Actor)>> •
<<backendfs2_actor, Backend FS2 (Actor)>>
====

====== UC05 – Check File Status
.Click to Open
[%collapsible]
====
.UC05 – Check File Status
[[uc5]]

Brief description::
Backend FS1 User may periodically refresh his view of Backend FS1 so he
gets an update on the status of files sent using UC04. The sending
status is indicated by the names and locations of the various content
files which the FS Plugin modifies according to the rules and states
described in section 3.5 - Check File Status Sequence.

Actors:: Backend FS1 User

Preconditions::

* The content file is successfully submitted to Domibus (C2) as an AS4
message.

Basic flow event::

1. The Backend FS1 User lists the existing files under the outgoing
folder of C1 until the observed files are successfully or failed sent.
2. The FS Plugin (C2) renames the processed files according to the status
change event for each message, see section 3.5 - Check File Status
Sequence.
3. The FS Plugin (C2) receives a Send Success event and deletes or
archives the processed file (according to the FS Plugin configuration).

Alternative flows::

* 3a. The FS Plugin (C2) receives a Send Failed event and deletes or
archives the processed file (according to the FS Plugin configuration).

Exception flows::

* 2a. The FS Plugin (C2) cannot access and rename the processed file.

* 2a1. The FS Plugin (C2) logs the rename error.

* 3b. The FS Plugin (C2) cannot access and rename the successfully sent file.

* 3b1. The FS Plugin (C2) logs the rename error.

* 3a1. The FS Plugin (C2) cannot access and rename the failed sent file.

* 3a1b. The FS Plugin (C2) logs the rename error.

Post conditions::

* *Successful conditions:* The content file is successfully submitted as an
AS4 message and it is successfully deleted or archived.

* *Failure conditions:* The content file name is not properly renamed
according to the message status change events; the event is registered
in the log files.

<<fsplugin_usecases, Top>> •
<<fsplugin_actor, FS Plugin (Actor)>> •
<<backendfs1_actor, Backend FS1 (Actor)>> •
<<backendfs2_actor, Backend FS2 (Actor)>>
====

====== UC06 – Receive Files
.Click to Open
[%collapsible]
====
.UC06 – Receive Files
[[uc6]]

Brief description::
Backend FS2 User initiates this Use Case by listing the files within the
incoming folder of any domain configured for the FS Plugin on Backend
FS2. Assuming files were successfully received and are present, the user
may obtain any of them via regular file system operations.

For detailed information about the Receive Files sequence, see chapter
3.6.

Actors::
Backend FS2 User

Preconditions::
* The content file is successfully sent from Access Point (C2) to (C3)
as an AS4 message.

Basic flow event::

1. The FS Plugin downloads the received AS4 message from the Access Point
(C3).
2. The FS Plugin resolves the destination domain from the AS4 metadata
Service and Action values.
3. The FS Plugin creates the received AS4 message as a file in the
resolved incoming folder of the Backend filesystem (C4).
4. The Backend FS2 User lists the received files under the incoming
folder of C4.

Alternative flows::
N/A

Exception flows::
* 2a. If no domain is found to match the pair Service/Action, the main
location is selected as the destination folder.

* 3a. The FS Plugin (C3) can’t access and create the received message as a
file.

* 3a1. The FS Plugin (C3) logs the access error and marks the transaction
for rollback. The deliver message operation will be retried by the
receiving Access Point.

Post conditions::
* *Successful conditions*: The content file is successfully received as an
AS4 message.

* *Failure conditions*: The fail message is logged by the FS Plugin.

<<fsplugin_usecases, Top>> •
<<fsplugin_actor, FS Plugin (Actor)>> •
<<backendfs1_actor, Backend FS1 (Actor)>> •
<<backendfs2_actor, Backend FS2 (Actor)>>
====
