:imagesdir: images/fsplugin/interface/

==== Behavioural Specification

//Children topics in behavconfig dir

:imagesdir: content/_common/images/fsplugin/interface/

include::behavspec/behavspec_fspluginconfig.adoc[]
//
include::behavspec/behavspec_supportedfs.adoc[]
include::behavspec/behavspec_metadataformat.adoc[]
include::behavspec/behavspec_sendfileseq.adoc[]
include::behavspec/behavspec_checkfileseq.adoc[]
include::behavspec/behavspec_receivefileseq.adoc[]


