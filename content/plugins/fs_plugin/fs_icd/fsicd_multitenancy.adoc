[[fsplugin_multitenancy]]
==== Multitenancy

The Default FS Plugin can be used when Domibus is configured in
Multitenancy mode.

In Multitenancy mode the plugins security is activated by default,
regardless of the configured value in *domibus.properties* for the
*domibus.auth.unsecureLoginAllowed* property.

As a result, every request to Domibus to send a file must be
authenticated via plugin username and password, which are configured in
*fs-plugin.properties* per domain. Please find below a configuration
example for domain *DOMAIN1*:

[source, properties]
----
# Mandatory in Multitenancy mode.
# The user that submits messages to Domibus.
# It is used to associate the current user
# with a specific domain.
fsplugin.domains.DOMAIN1.authentication.user=

# Mandatory in Multitenancy mode.
# The credentials of the user defined under the property username.
fsplugin.domains.DOMAIN1.authentication.password=
----

For more information on how to create plugin users used for authentication, see <<adminconsole_pluginusers>>.
