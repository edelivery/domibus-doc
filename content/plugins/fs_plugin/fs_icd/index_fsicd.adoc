
//ICD Document
[[fsplugin_interface]]
=== FS Plugin Interface

Here we outline the file system messages exchange as part of the default File System (FS) backend integration solution for the Domibus Access Point[multiblock footnote omitted].

Here we covers the service interface of the Access Point from the
perspective of the File System backend integration.

It includes information regarding the description of:

* FS Plugin Configuration
* Supported File Systems
* Folders Structure
* Send and Receive Workflow

:imagesdir: content/_common/images/fsplugin/interface/

include::fsicd_about.adoc[]
include::fsicd_funcspec.adoc[]
include::fsicd_behavspec.adoc[]
include::fsicd_notifications.adoc[]
include::fsicd_multitenancy.adoc[]