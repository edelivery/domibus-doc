[[fsplugin_domainprops]]
===== Domain specific properties

in which the domains will be evaluated. This property is not mandatory –
domains without order definition will be resolved randomly after the
ordered domains. The order is defined with numeric values greater than
0. E.g.: 1

[width="100%",cols="28%,16%,56%",options="header"]
|===
|Property name |Default value |Description
|`fsplugin.domains.<domain_id>.messages.expression` |(none) |Regular
expression used to match the domain for the reception of messages. This
regular expression will be evaluated against the Service and Action
values from the incoming message separated by #. E.g.:DOMAIN1SampleService.

|`fsplugin.domains.<domain_id>.messages.location` |(none) |The location of
the folder that the plugin will use to manage the messages to be sent
and received in case no domain expression matches. This location must be
accessible to the Domibus instance. The domain locations must be
independent from each other and should not overlap. For more information
about the location format and values see section 3.2 - Supported File
Systems. E.g.: `/home/domibus/fs_plugin_data/DOMAIN1`.

|`fsplugin.domains.<domain_id>.messages.user` |(none) a|
The user used to access the domain location specified by property :
`fsplugin.domains.<domain_id>.messages.location`.

This value must be provided if the location access is secured at the
file system level so that users from other domains cannot access its
contents. In a secured File System, e.g.: SFTP, if the credentials are
not provided, the FS Plugin will not be able to access it to perform the
file messages exchange. For more information see section 3.2 - Supported
File Systems.

|`fsplugin.domains.<domain_id>.messages.password` |(none) |The password
used to access the domain location. This value must be provided if the
location access is secured at the file system level.

|`fsplugin.domains.<domain_id>.authentication.user` |(none) |Mandatory in
Multitenancy mode. The user that submits messages to Domibus. It is used
to associate the current user with a specific domain.

|`fsplugin.domains.<domain_id>.authentication.password` |(none) |Mandatory
in Multitenancy mode. The credentials of the user defined under the
property username.

|`fsplugin.domains.<domain_id>.messages.payload.id` |cid:message |The
payload identifier for messages processed on a particular domain.

|f``splugin.domains.<domain_id>.send.queue.concurrency`` |5-20 |Specify
queue concurrency limits on a particular domain when sending files via a
"lower-upper" String, e.g. "5-10", or a simple upper limit String, e.g.
"10" (the lower limit will be 1 in this case)

|`fsplugin.domains.<domain_id>messages.send.delay` |2000 |The delay (in
milliseconds) on a particular domain to allow the writing process to
finish writing.
|===

