[[]]
===== Domain resolution

In order to support multiple domains configurations, the domain
resolution is done according to the values of the domain specific
properties:

* `fsplugin.domains.<domain_id>.order`
Defines the order in which the domains will be evaluated.
* `fsplugin.domains.<domain_id>.messages.expression`
Regular expression used to match the domain for the reception of messages. This regular
expression will be evaluated against a concatenation of the Service and
Action values from the incoming message using the character # as
separator, for example:

`BRISReceptionService#SendEmailAction`

An expression per domain will define to which domain the message is
intended for. As a convention for the PMode, it is recommended to prefix
the Service with the identifier of the business domain.

.Example:
`fsplugin.domains.BRIS.messages.expression=BRISReceptionService`
This regular expression means that all messages containing AS4 headers
where the Service is set to BRISReceptionService and having any Action
value will be mapped into the domain BRIS.
A sample match is `BRISReceptionService#WelcomeAction`.

NOTE: Java Regular Expression Syntax reference: +
https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html.

[width="100%",cols="28%,16%,56%",options="header"]
|===
|Property name |Default value |Description
|`fsplugin.domains.<domain_id>.order` |(none) |Integer defining the order
|===
