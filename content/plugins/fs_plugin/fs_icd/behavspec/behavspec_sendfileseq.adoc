[[fsplugin_sendfiles]]
===== Send Files Sequence

image:image10.png[width=604,height=480]

.Send Files sequence diagram
The FS Plugin (C2) continually polls the various configured locations
for a list of existing files in the outgoing folder and subfolders,
multiple files can be found simultaneously in the same folder. It then
filters that list by excluding metadata files, content files that have
been processed previously (those which file name contains a message id)
and files with .lock suffix to create a set of eligible content files.
For each eligible content file, the plugin uses the metadata file named
"metadata.xml" within the same directory than the considered content
file.

For each content file, the plugin put a message to an internal queue –
FSPluginSendQueue and the messages are consumed from JMS queue by
several consumers – the numbers of these consumers could be changed in
fs-plugin.properties , property:

* `fsplugin.send.queue.concurrency=5-20`

The value could be changed as well per domain:

* `fsplugin.domains.DOMAIN1.send.queue.concurrency=5-20`

The name of the queue could be changed in the property:

* `fsplugin.send.queue=domibus.fsplugin.send.queue`

For each message in the JMS queue, the plugin then creates an AS4
message for each eligible file by retrieving information from metadata
file ( `metadata.xml` within the same directory) to populate the message
properties and adding the content file as AS4 payload. The `MIME` type for
the payload is derived from the content file's extension (or file
content itself if the file has no extension) according to the supported
formats[multiblock footnote omitted]. This message is then submitted to
the Domibus core and the content file is renamed by adding the message
id to the original file name, as follow:

* `originalName_messageId.originalExtension`

On its own schedule, Domibus core (C2) eventually connects to the
applicable destination gateway (C3) to deliver the message.
