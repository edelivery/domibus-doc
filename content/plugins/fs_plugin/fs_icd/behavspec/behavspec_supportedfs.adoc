[[fsplugin_supportedfs]]
===== Supported File Systems

image:image5.png[width=316,height=240]

.FS Plugin - VFS abstraction
The FS Plugin supports multiple file system types via Apache VFS. There
are 4 file systems currently supported:

* <<fsplugin_local>>
* <<fsplugin_smbcifs>>
* <<fsplugin_sftp>>
* <<fsplugin_ftp>>

.Click to Open
[%collapsible]
====
.Local
[[fsplugin_local]]

A local file system is simply a directory on the local physical system.
The URI format is:

* `[file://]absolute-path`

Where `absolute-path` is a valid absolute directory name on the local
platform. UNC names are supported under Windows.

This type of file system does not support authentication hence the
domibus user needs read/write access to this directory.

.Examples:

* `\file:///home/someuser/somedir`
* `\file:///C:/Documents and Settings`
* `/home/someuser/somedir`
* `c:\program files\some dir`
* `c:/program files/some dir`
====

.Click to Open
[%collapsible]
====
.SMB/CIFS
[[fsplugin_smbcifs]]

A SMB/CIFS file system is a remote directory shared via Samba or Windows
Share, with the following URI format:

* `smb://hostname[:port]/sharename[/relative-path]`

Notice that a share name is mandatory.

This type of file system supports authentication via user and password
domain properties. See <<behavspec_config>>.

.Examples:

* `smb://somehost/shareA`
* `smb://somehost/shareB/nesteddir`
* `smb://otherhost:445/shareC`
====

.Click to open
[%collapsible]
====
.SFTP
[[fsplugin_sftp]]

An SFTP file system is a remote directory shared via SFTP. Uses an URI
of the following format:

`sftp://hostname[:port][/relative-path]`

The path is relative to whatever path the SFTP server has configured as
base directory, usually the user's home directory.

This type of file system supports authentication via user and password
domain properties. See <<behavspec_config>>.

.Examples:

* sftp://somehost/pub/downloads/
* sftp://somehost:22/pub/downloads/
====

.Click to open
[%collapsible]
====
.FTP
[[fsplugin_ftp]]

An FTP file system is a remote directory shared via FTP. Accepts URIs of
the following format:

* `\ftp://hostname[:port][/relative-path]`

The path is relative to whatever path the FTP server has configured as
base directory, usually the user's home directory.

This type of file system supports authentication via user and password
domain properties. See <<behavspec_config>>.
.Examples:

* `\ftp://somehost/pub/downloads/`

Due to incompatibilities between the current version of VFS and certain
FTP servers on Linux (e.g.: vsftpd), using a relative path prevents some
of the plugin's functionality from working correctly. For that reason,
in those cases an absolute path must be specified, e.g.:

* `\ftp://somelinuxhost/home/someuser/pub/downloads/`

====

