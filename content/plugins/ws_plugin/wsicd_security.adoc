[[wsplugin_security]]
==== Security

===== Authentication

The default *WS Plugin* implements authentication and authorization.
By default, the plugin's security is disabled and all the methods of the
plugin can be called with no authentication credentials.

The *WS Plugin* supports three authentication methods:

* Basic Authentications
* X509 Certificates Authentication
* Blue Coat Authentication

NOTE: Blue Coat is the name of the reverse proxy at the Commission. It
forwards the request in HTTP with the certificate details inside the
request (`Client-Cert` header key).

Basic authentication is the most common used method used for the
WS Plugin. An existing user defined in the *Plugin User* UI
page can authenticate with basic authentication and call any operation
of the WS Plugin. +
More details on how to create plugin users used for basic authentication see <<adminconsole_pluginusers>>.

The WS Plugin uses a custom interceptor `eu.domibus.plugin.webService.impl.CustomAuthenticationInterceptor`
to intercept the incoming requests and perform authentication. Once the
request is intercepted the `CustomAuthenticationInterceptor` delegates
the authentication to the service `eu.domibus.ext.services.AuthenticationExtService` provided by the Plugin API.

===== Authorization

The WS Plugin uses the authorization mechanism described in <<plugin_authorization>>.

.Default users
There are two default users already inserted in the database.

NOTE: Make sure you already ran the migration scripts.

These pre-defined users are:

* `admin`, with the role `ROLE_ADMIN`
* `user`, with the role `ROLE_USER`

.About the users roles

`ROLE_ADMIN` has the permission to call:

* `submitMessage`, with any value for the `originalSender` property.
* `retrieveMessage`, any message among messages notified to this plugin.
* `listPendingMessages` lists all pending messages for this plugin
* `getStatus` and `getStatusWithAccessPointRole`
* `getMessageErrors` and `getMessageErrorsWithAccessPointRole`
* `markMessageAsDownloaded`
* `listPushFailedMessages`, with any value for the `originalSender` property
* `rePushFailedMessages`

`ROLE_USER` has the permission to call:

* `submitMessage` with `originalSender` equal to the `originalUser`
* `retrieveMessage`, only if `finalRecipient` equals the `originalUser`
* `listPendingMessages`, only messages with `finalRecipient` equal to the `originalUser`.
* `getStatus`, `getStatusWithAccessPointRole` and `getErrors` for its own messages
* `markMessageAsDownloaded`
* `listPushFailedMessages`, only if `finalRecipient` equals the `originalUser`
* `rePushFailedMessages`
