[[wsplugin_interface]]
=== WS Plugin Interface

.About this content
[%collapsible]
====
This content defines the participant’s interface to the
Access Point (Corner Two and Corner Three in the four-corner topology
that will be explained later in this document) component of the
eDelivery building block. This document describes the WSDL and the
observable behaviour of the interface provided by Domibus and included
in the default-ws-plugin.

This section describes the WSDL and the
observable behaviour of the interface provided by Domibus 4.x.y and
included in the default-ws-plugin.

Here you can find information to understand the Access Point
(Corner Two and Corner Three in the four-corner model)
services provided by Domibus delivered by eDelivery.

There is one interface described in this document:

.Actor list
[width="100%",cols="35%,65%",options="header"]
|===
|Interface |Description
|WebServicePlugin.wsdl
|The webservices interface for Domibus WS default plugin
|===
====

.Scope
[%collapsible]
====
This content covers the service interface of the Access Point. It
includes information regarding the description of the services
available, the list of use cases, the information model and the sequence
of message exchanges for the services provided. This specification is
limited to the service interface of the Access Point. All other aspects
of its implementation are not covered by this document.
====

.Audience
[%collapsible]
====
The intended target audience for this guide are:

* The Directorate Generals and Services of the European Commission,
Member States (MS) and also companies of the private sector wanting to
set up a connection between their backend systems and the Access Point.
In particular:
** Architects will find it useful for determining how to best exploit
the Access Point to create a fully-fledged solution and as a starting
point for connecting a Back-Office system to the Access Point.
** Analysts will find it useful to understand the Access Point that will
enable them to have an holistic and detailed view of the operations and
data involved in the use cases.
** Developers will find it essential as a basis of their development
concerning the Access Point plugin services.
** Testers can use this document in order to test the interface by
following the use cases described.
====

//Notes about this edit
// Each guide docx has its specific references table, these are additional sources of information but it is very rare that they are mentioned in the document itself, so they are not cross-references really.
//course of action is to turn is to replace it as a link whenever it could be a cross-reference so when for example REF1 (this doc) is mentioned anywhere else in the document beside the References Table itself at the top of the doc
// Otherwise remove all "REF#" as they are not cross references but additional info resources


.Useful Resources
[%collapsible]
====
Below you can find useful information sources:

* https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Access+Point+software[Overview of eDelivery Access Point]
+
Access Point - Component Offering Description (REF1)
//Replace with AP COD when merge is OK

* http://www.restapitutorial.com/lessons/httpmethods.html[HTTP Methods for RESTful Services]
+
Short descriptions and using HTTP Methods for RESTful Services

* https://www.oasis-open.org/committees/download.php/40926/PEPPOL%20D8_2%20-%20Attachment%20I%20%20BusDox%20Common%20Definitions.pdf[Business Document Metadata Service Location]
+
BDMSL Software Architecture Document
+
This document is the Software Architecture document of the
CIPA eDelivery Business Document Metadata Service Location application
(BDMSL) sample implementation. +
It intends to provide detailed information about the project:

1. An overview of the solution
2. The different layers
3. The principles governing its software architecture

* http://www.ebxml.org/[ebXML]
+
About the Electronic Business using eXtensible Markup Language(ebXML)

* http://www.w3.org/TR/wsdl[Web Services Description Language (WSDL) 1.1]
+
WS-I Basic Profile Version 1.1

* http://www.w3.org/XML/Schema[XML Schema 1.1]

* https://www.w3.org/TR/xml11/[Extensible Markup Language (XML) 1.1]
// broken link https://urldefense.com/v3/
// https:/www.w3.org/TR/2006/REC-xml11-20060816/__;!!DOxrgLBm!XCk-8JeWkikuGpPzmth-Yid_wc6woN3doZ6bQg2R65M9i8Zn-qiUBix4LwIvSDqjdDeY1_ZGYEI$
// replaced https://www.w3.org/TR/xml11/

* http://www.ietf.org/rfc/rfc2616.txt[Hypertext Transfer Protocol 1.1]

* http://www.w3.org/TR/SOAP-attachments[SOAP Messages with Attachments]

//https://urldefense.com/v3/ - broken link
* http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/profiles/AS4-profile/v1.0/AS4-profile-v1.0.html[AS4 Profile of ebMS 3.0 Version 1.0]
// http:/docs.oasis-open.org/ebxml-msg/ebms/v3.0/profiles/AS4-profile/v1.0/os/AS4-profile-v1.0-os.html__;!!DOxrgLBm!XCk-8JeWkikuGpPzmth-Yid_wc6woN3doZ6bQg2R65M9i8Zn-qiUBix4LwIvSDqjdDeYhZpvy1o$ is broken
// replaced with http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/profiles/AS4-profile/v1.0/AS4-profile-v1.0.html

* http://wiki.ds.unipi.gr/display/ESENS/PR+-+AS4[e-SENS AS4 Profile 1.11]
* https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/eDelivery+AS4[eDelivery AS4 profile]

* <<config_pmode, eDelivery Pmode Configuration>>

* http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/core/ebms-header-3_0-200704.xsd[XSDs for ebms3]

* http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/core/cs02/ebms_core-3.0-spec-cs-02.pdf[ebXML]
+
Electronic Business using eXtensible Markup Language (ebXML)
====

.Overview
Services' implementers and consumers can find a complete specification of the WS Plugin:

* <<wsplugin_functionalspec>>, this specifies the set of
services and the operations provided by each service and this is
represented by the flows explained in the use cases.
* <<wsplugin_behavioralspec>>, this specifies the expected
sequence of steps to be respected by the participants in the
implementation when calling a service or a set of services and this is
represented by the sequence diagrams presented in the use cases.
* <<wsplugin_msgstandards, Message Standards>>, this specifies the syntax and semantics of the data.
