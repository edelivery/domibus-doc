[[wsplugin_usecases]]
===== Use Cases Overview

[caption=,title=Actors]
[width="100%",cols="20%,80%",options="header"]
|===
|Actor |Definition
|Backend C1 |Any participant submitting messages to any other Backend C4
and using the Sending AP C2 for that purpose.
|Backend C4 |Any participant retrieving messages from any other Backend
C1 and using the Receiving AP C3 for that purpose.
|===

NOTE: Greyed use cases in this paragraph show deprecated
operations in the WSDL (in these diagrams, the use cases below these
replace them). Since deprecated and replacing operations have the same
functionality (only technical changes), in each case only one use case
is presented for both.

[caption=,title=Backend C1 Use Cases]
[width="100%",cols="8%,20%,30%,10%,12%",options="header"]
|===
a|*ID* |*UC* |*Description* |*Operation* |*System*
a|*<<uc1_ws, UC01>>* |*Submit Message*
|Submit any type of document from a Backend C1 to a Backend C4
|submitMessage
|Domibus 4.x.y
a|*<<uc3_ws, UC03>>* |*Get Status of the Message*
|Get the status of the Message with messageId and access point role.
a|
`getStatusWithAccessPointRole`
|Domibus 5.1
|===

.Backend C1 Use Cases Diagram
image:image2a.png[width=525,height=225]

[caption=,title=Backend C4 Use Cases]
[width="100%",cols="8%,20%,30%,10%,12%",options="header"]
|===
|*ID* |*UC* |*Description* |*Operation* |*System*
|*<<uc2_ws, UC02>>* |*Download Message* |Retrieve the message from the Receiving AP C3 |retrieveMessage |Domibus 4.x.y
|*<<uc2_ws, UC02>>* |*Get Status of the Message* |Get the status of the Message|getStatus |Domibus 4.x.y
|*<<uc4_ws, UC04>>* |*ListPending Messages* |Check the pending messages to be retrieved by the Backend C4 from C3 |listPendingMessages |Domibus 4.x.y
|===

.Backend C4 Use Cases Diagram
image:image2.png[width=544,height=443]

===== Use Cases Detail

The following paragraphs define the use cases listed above with more
detail.

The _Interface Functional Specification_ is described in the detailed
uses cases using the Request and the Response examples. It is important
to remark that the Inputs and Responses provided as examples for the
use cases are based on a specific PMode configuration.

As defined in the
https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/eDelivery+AS4[eDelivery Specification Library],
a PMode is the contextual information that governs the processing of
a particular message (thus is basically a set of configuration parameters).
The PMode associated with a message determines, among other things,
which security and/or which reliability protocol and parameters,
as well as which MEP (Message Exchange Pattern)
is being used when sending a message. The technical representation of
the PMode configuration is implementation dependent. +
C1 and C4 may be one or more participants.

The state machine diagrams presented below depict the various states in
which a message may be during its lifecycle when submitting or
downloading the message. These are presented to have a more
comprehensive vision of the process that the messages go through. It is
also important to remark that also the sequence diagram of the basic
flow is presented in the use cases.

The state machine diagram for submitting the message in C2:

.C2 State machine
image:image3.png[width=604,height=187]

The state machine diagram for downloading the message in C3:

.C3 State machine
image:image4.png[width=604,height=119]

====== UC01 Submit Message
.Click to Open
[%collapsible]
====
.UC01 Submit Message
[[uc1_ws]]

Brief description::
Submit any message with attachments from Backend C1 to the Backend C4.
The response from C2 to C1 is synchronous and contains a messageId and message entity id. +
MTOM feature is required when sending large files so that the
attachments are send outside the XML envelope, as parts. Otherwise, the
attachments will be encoded base64 and sent inside the envelope and the
maximum limit would be 128Mb. +
The state machine of the outgoing messages is the following:
+
.Sequence Diagram C1 to C2 – SubmitMessage
image:image5.png[width=604,height=335]

[caption=, title=Actors]
[width="100%",cols="10%,90%",options="header"]
|===
2+|*Actors*
|C1|Backend C1
|C2|Access Point C2
|C3|Access Point C3
|===

[caption=, title=Preconditions]
[width="100%",cols="10%,90%",options="header"]
|===
2+|*Preconditions*
|C1|Backend C1 has a message to submit.
|C1|The message is valid. A message is valid if it respects the
message standard format (*See* ☞ <<wsplugin_msgstandards>>).

|C2,C3|The Sending AP (C2) and the Receiving AP (C3) are up and
running and properly configured.
|===

[caption=, title=Basic Flow]
[width="100%",cols="10%,10%,50%,15%,15%",options="header"]
|===
|*Actor*|*Step*|*Description*
^|*C2* +
*Message State*
^|*C3* +
*Message state*
|*C1*|1|Backend C1 submits the message.|N/A |-
|*C2*|2|C2 sends an ACK to C1 containing the ID of the message.|SEND_ENQUEUED|-
|*C2*|3|The message directly passes through to SEND_ENQUEUED, meaning that it
is available for processing. All messages go through this step regardless of load.|SEND_ENQUEUED|-
|*C2*|4|Once the sending process finishes the status changes to WAITING_FOR_RECEIPT.|WAITING_FOR_RECEIPT|-
|*C3*|5|Once the reception is finished by C3, the status changes to RECEIVED.|WAITING_FOR_RECEIPT|RECEIVED
|*C3*|6|The Receiving AP (C3) responds ACK to C2|WAITING_FOR_RECEIPT|RECEIVED
|*C2*|7|The status of the message changes to ACKNOWLEDGED. +
If configured in the PMode for non-repudiation, the receipt SHOULD
contain a single ebbpsig:NonRepudiationInformation child element. The
value of eb:MessageInfo/eb:RefToMessageId MUST refer to the message for
which this signal is a receipt.|ACKNOWLEDGED|RECEIVED
|-|8 |Use case ends in successful condition.|-|-
|===

[caption=, title=Exception Flow]
[width="100%",cols="10%,10%,60%,10%,10%",options="header"]
|===
|*Actor*|*Step*|*Description*
^|*C2* +
*Message* +
*State*
^|*C3* +
*Message state*

h|*C2* h|*E2.1* h|*The ID provided by C1 already exists* h|- h|-

|C2|E2.1.1 .2+|The `parameterPMode[1].ReceptionAwareness.DuplicateDetectionmust` be set to TRUE
The status of the message changes to SEND_FAILURE. .2+|SEND_FAILURE .2+|-
|C2|E2.1.2

h|*C2* h|*E11.1* h|*Wrong receipt received or any other failure (e.g connection lost)* h|- h|-
|C2|E11.1.1|The status changes to WAITING_FOR_RETRY.|WAITING_FOR_RETRY|-
|C2|E11.1.2|Continue to step 3|-|-

h|*C2* h|*E11.1.3.1* h|*The maximum number or retries (Configurable via PMode on a by-usecase basis) has been reached*  h|- h|-
|C2|E11.1.3.1.1|The status of the message changes to SEND_FAILURE.|SEND_FAILURE|-
|C2|E11.1.3.1.1|A notification can be sent to the Backend C1 that initially submitted the message.|SEND_FAILURE |
|C2|E11.1.3.1.3|Use case ends in failure condition.|-|-
|===

[caption=,title=Post conditions]
[width="100%",cols="10%,90%"]
|===
h|*Successful conditions*
|The operation is a success if getStatus in C2 is ACKNOWLEDGED
and this means that the Receiving AP (C3) has received the message
submitted by the Backend C1 and the status in C3 is RECEIVED. The method
getStatus must be called with the identifier of the message received in
the response or specified in the request.
h|*Failure Conditions*
|Errors may be sent as SOAP Fault or as http:5XX.
|===

.Request Example
[source,xml]
----
<soap:Envelope
    xmlns:soap="http://www.w3.org/2003/05/soap-envelope"
    xmlns:ns="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/"
    xmlns:_1="http://eu.domibus.wsplugin/">
    <soap:Header>
        <ns:Messaging> +
# <#ns:UserMessage## mpc=”
http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC”>
            <ns:PartyInfo>
                <ns:From>
                    <ns:PartyId
type="urn:oasis:names:tc:ebcore:partyid-type:unregistered">domibus-blue</ns:PartyId>
                    <ns:Role>http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator</ns:Role>
                </ns:From>
                <ns:To>
                    <ns:PartyId
type="urn:oasis:names:tc:ebcore:partyid-type:unregistered">domibus-red</ns:PartyId>
                    <ns:Role>http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder</ns:Role>
                </ns:To>
            </ns:PartyInfo>
            <ns:CollaborationInfo>
                <ns:Service type="tc1">bdx:noprocess</ns:Service>
                <ns:Action>TC1Leg1</ns:Action>
            </ns:CollaborationInfo>
            <ns:MessageProperties>
                <ns:Property
name="originalSender">urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1</ns:Property>
                <ns:Property
name="finalRecipient">urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4</ns:Property>
            </ns:MessageProperties>
            <ns:PayloadInfo>
                <ns:PartInfo href="cid:message">
                    <ns:PartProperties>
                        <ns:Property name="MimeType">text/xml</ns:Property>
                    </ns:PartProperties>
                </ns:PartInfo>
            </ns:PayloadInfo>
        </ns:UserMessage>
    </ns:Messaging>
</soap:Header>
<soap:Body>
    <_1:submitRequest>
        <|--Optional-->
        <bodyload>
            <value>cid:bodyload</value>
        </bodyload>
        <payload payloadId="cid:message" contentType="text/xml">
            <value>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=</value>
        </payload>
    </_1:submitRequest>
</soap:Body>undefined</soap:Envelope>undefined<soap:Body>
<_1:submitRequest>
    <payloadpayloadId="cid:message"contentType="text/xml">
        <value>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=</value>
    </payload>
    <payloadpayloadId="cid:attachment"contentType="application/octet-stream">
        <value>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=</value>
    </payload>
</_1:submitRequest>undefined</soap:Body>undefined</soap:Envelope>
----

.Response Example
[source,xml]
----
<soap:Envelopeundefinedxmlns:soap="http://www.w3.org/2003/05/soap-envelope">undefined<soap:Body>
<ns5:submitResponse
    xmlns:ns6="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/"
    xmlns:ns5="http://eu.domibus.wsplugin"
    xmlns:xmime="http://www.w3.org/2005/05/xmlmime">
    <messageID>23dce7d9-2781-4623-beeb-6b43ab9e7d37@domibus.eu</messageID>
</ns5:submitResponse>undefined</soap:Body>undefined</soap:Envelope>
----

.Special requirements
* N/A
====

====== UC02 Retrieve Message
.Click to Open
[%collapsible]
====
.UC02 - Retrieve Message
[[uc2_ws]]

Brief description::
Retrieve any type of message sent from Backend C1 to Backend C4. The
retrieval of the message is based on a PULL mechanism. C4 downloads the
message from C3.
+
Please note that `retrieveMessage` method replaces the deprecated method
downloadMessage to support the retrieval of large files. MTOM feature is
required when retrieving large files.
+
.Sequence Diagram C4 to C3 – retrieveMessage
image:image6.png[width=227,height=112]

[caption=,title=Actors]
[width="100%",cols="10%,90%",options="header"]
|===
2+|*Actors*
|C3|Access Point C3
|C4|Backend C4
|===

[caption=,title=Preconditions]
[width="100%",cols="10%,90%",options="header"]
|===
2+|*Preconditions*
|C3|There is at least one message sent by AP C2 and successfully received in the Receiving AP C3.
|C3|The Receiving AP (C3) is up and running and properly configured.
|C4|C4 Has previously requested information about pending messages from C3. +
C3 has returned a response containing the messageID('s) of the message(s) received (cf. UC04).
|===

[caption=,title=Basic Flow]
[width="100%",cols="10%,10%,50%,30%",options="header"]
|===
|*Actor*|*Step*|*Description*|*C3 Message State*
|C4|1|Requests, to the Receiving AP C3, the service `retrieveMessage`
by providing the `messageId` and the attribute `markAsDownloaded` with the
value true (which is the default value when this optional attribute is
missing)|RECEIVED

|C3|2|Receiving AP C3 retrieves and sends the information
retrieveMessageResponse to C4 as response to his request. This is the
payload (message content and attachments) and metadata, analogous to the
message sent from C1 to C2 in UC01. The status of the message changes to
DOWNLOADED when the message is retrieved by the Backend C4. |DOWNLOADED

|C4|3|Receives the message as sent by C1 |DOWNLOADED

|C3|4|Deletes the payload of the message from the database if
retention timeout for downloaded messages = 0. +
NB: While the message metadata is still recoverable by a Domibus
administrator all payload data is purged. This is necessary to be able
to prove message exchanges in case of disputes. It is possible to
produce the signature of a payload but not the payload itself. |DELETED

|C3|5|The status of the message changes to DELETED
when the message is deleted by C3 after the configured retention timeout
for downloaded messages (_retention_downloaded_) expired. Note: If
_retention_downloaded_ has a negative value, the message will never be
deleted from C3. If the message was not downloaded, the
_retention_undownloaded_ value will be used as timeout for deletion.
|DELETED
|6|Use case ends |DELETED
|===

[caption=,title=Alternative Flow]
[width="100%",cols="10%,10%,50%,30%",options="header"]
|===
|*Actor*|*Step*|*Description*|*C3 Message State*
|C3|A1.1|*Configured retention time has passed*|RECEIVED
|C3|A1.1.1|Go directly to step 4|RECEIVED
|===

[caption=,title=Alternative Flow 2]
[width="100%",cols="10%,10%,50%,30%",options="header"]
|===
|*Actor*|*Step*|*Description*|*C3 Message State*
|C4|A2.1|Requests, to the Receiving AP C3, the service
`retrieveMessage` by providing the messageID and the attribute
`markAsDownloaded` with the value `False`. |RECEIVED

|C3|A2.2|Receiving AP C3 retrieves and sends the information
`retrieveMessageResponse` to C4 as response to his request. This is the
payload (message content and attachments) and metadata, analogous to the
message sent from C1 to C2 in UC01.|RECEIVED

|C4|A2.3|Receives the message as sent by C1. |RECEIVED

|C4|A2.4|Requests, to the Receiving AP C3, the service
`markMessageAsDownloaded` by providing the `messageId`. |RECEIVED

|C3|A2.5|C3 responds with a confirmation message containing the
`messageID` if the message was not marked as DOWNLOADED until then or an
error message otherwise. The status of the message changes to
DOWNLOADED when the message is retrieved by the backend
C4.|DOWNLOADED

|C3|A2.6|Go to step 4 |DELETED
|===

[caption=,title=Alternative Flow 2]
[width="100%",cols="10%,10%,50%,30%",options="header"]
|===
|*Actor*|*Step*|*Description*|*C3 Message State*
h|*C3* h|*E2.1* h|*Wrong messageID (malformed or missing), this is a condition of failure.*
h|*(NOT FOUND)*
|C3|E2.1.1 |The `NOT FOUND` status is a pseudo state for messages that
are not available for download (were never received or were rejected).
|(NOT FOUND) |
|C3|E2.1.2 |Use case ends in failure condition |(NOT FOUND)
|===

[caption=,title=Post conditions]
[width="100%",cols="10%,90%"]
|===
h|*Successful conditions*
|It is a success if the Message Status is `ACKNOWLEDGED` on
C2 and `DOWNLOADED` or `DELETED` on C3. +
Payload of the message may be deleted from C3's database.

h|*Failure Conditions*
|No message payload is returned to C4. The response contains a
description of the encountered error. The operation is not a success if
`GetStatus` is `SEND_FAILURE` on C2 and `NOT FOUND` on C3 and this means that the Backend C4 has not been able to
download the message submitted by the Backend C1. If the `retrieveMessage`
method is called with a wrong messageID (malformed or missing), this is
a condition of failure. The `NOT FOUND` status is a pseudo state for messages
that are not available for download (were never received or were rejected).
|===

.Request Example
[source, XML]
----
<soap:Envelope
    xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:_1="
    http://eu.domibus.wsplugin/">
    <soap:Header/>
    <soap:Body>
        <_1:retrieveMessageRequest>
            <messageID>$\{ResponseParameters#messageID}</messageID>
        </_1:retrieveMessageRequest>
    </soap:Body>
</soap:Envelope>
----

.Response Example
[source, XML]
----
<soap:Envelope
    xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
    <soap:Header>
    <ns6:Messaging mustUnderstand="false"
        xmlns:ns6="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/"
        xmlns:ns5=" http://eu.domibus.wsplugin/"
        xmlns:xmime="http://www.w3.org/2005/05/xmlmime">
    <ns6:UserMessage> mpc=”http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC”>
    <ns6:MessageInfo>
        <ns6:Timestamp>2017-10-02T17:32:14.956+02:00</ns6:Timestamp>
        <ns6:MessageId>d05051c6-951c-4f40-90b5-459eca9d8302@domibus.eu</ns6:MessageId>
    </ns6:MessageInfo>
    <ns6:PartyInfo>
    <ns6:From>
    <ns6:PartyId
    type="urn:oasis:names:tc:ebcore:partyid-type:unregistered">domibus-blue</ns6:PartyId>
    <ns6:Role>http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator</ns6:Role>
    </ns6:From>
    <ns6:To>
    <ns6:PartyId
    type="urn:oasis:names:tc:ebcore:partyid-type:unregistered">domibus-red</ns6:PartyId>
    <ns6:Role>http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder</ns6:Role>
    </ns6:To>
    </ns6:PartyInfo>
    <ns6:CollaborationInfo>
    <ns6:Service type="tc1">bdx:noprocess</ns6:Service>
    <ns6:Action>TC1Leg1</ns6:Action>
    <ns6:ConversationId>52f1c57d-bd35-4ab2-a0a5-da9a15101dba@domibus.eu</ns6:ConversationId>
    </ns6:CollaborationInfo>
    <ns6:MessageProperties>
    <ns6:Property
    name="finalRecipient">urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4</ns6:Property>
    <ns6:Property
    name="originalSender">urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1</ns6:Property>
    </ns6:MessageProperties>
    <ns6:PayloadInfo>
    <ns6:PartInfo href="cid:message">
    <ns6:Schema/>
    <ns6:PartProperties>
    <ns6:Property name="MimeType">text/xml</ns6:Property>
    </ns6:PartProperties>
    </ns6:PartInfo>
    </ns6:PayloadInfo>
    </ns6:UserMessage>
    </ns6:Messaging>
    </soap:Header>
    <soap:Body>
        <ns5:retrieveMessageResponse
        xmlns:ns6="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/"
        xmlns:ns5=" http://eu.domibus.wsplugin/"
        xmlns:xmime="http://www.w3.org/2005/05/xmlmime">
        <payload payloadId="cid:message">
        <value>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=</value>
        </payload>
        </ns5:retrieveMessageResponse>
    </soap:Body>
</soap:Envelope>
----

Security::
In Multitenancy mode, the general property
*domibus.auth.unsecureLoginAllowed* is ignored and authentication is
always required.
For more about authentication options, see the Domibus Authentication section in the
<<adminguide, Administration>>.
====

====== UC03 Get the status of the Message
.Click to Open
[%collapsible]
====
.UC03 - Get the status of the Message
[[uc3_ws]]

Brief description::
Get the status of the Message sent from Backend C1 or received by the
Backend C4:
+
.Sequence Diagram – GetStatus
image:image7.png[width=604,height=88]

[caption, title=Actors]
[width="100%",cols="10%,90%",options="header"]
|===
2+|*Actors*
|C1|Backend C1
|C2|Access Point C2
|C3|Access Point C3
|C4|Backend C4
|===

[caption, title=Preconditions]
[width="100%",cols="10%,90%",options="header"]
|===
2+|*Preconditions*
| |There is at least one message sent by Backend C1 or to be retrieved by Backend C4.
| |The Sending AP C2 and the Receiving AP C3 are up and running and properly configured.
|===

.Basic flow event
1. Backend C1 or the Backend C4 launch a `statusRequest` using the `messageId` and access point role.
2. The Access Point (Sending AP C2 or Receiving AP C3) retrieve the `getStatusResponse`.
3. Use case ends.

.Exception flow
* N/A

[caption=,title=Post Conditions]
[width="100%",cols="10%,90%"]
|===
h|*Successful conditions*
|The operation is a success if `GetStatusResponse` retrieves any status of the following:

* `SEND_ENQUEUED`
* `WAITING_FOR_RECEIPT`
* `ACKNOWLEDGED`
* `SEND_FAILURE`
* `NOT_FOUND`
* `WAITING_FOR_RETRY`
* `RECEIVED`
* `DELETED`
* `DOWNLOADED`

h|*Failure Conditions*
|The message does not exist.
|===

.Special requirements
* N/A

Security::
In Multitenancy mode, the general property
*domibus.auth.unsecureLoginAllowed* is ignored and authentication is
always required. +
For more about authentication options, see the Domibus Authentication section in the
<<adminguide, Administration>>.

.Request Example
[source, XML]
----
<soap:Envelope
    xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:_1="
    http://eu.domibus.wsplugin/">
    <soap:Header/>
    <soap:Body>
        <_1:statusRequest>
            <messageID>d05051c6-951c-4f40-90b5-459eca9d8302@domibus.eu</messageID>
        </_1:statusRequest>
    </soap:Body>
</soap:Envelope>
----

.Response Example
[source, XML]
----
<soap:Envelope
    xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
    <soap:Body>
        <ns5:getStatusResponse
        xmlns:ns6="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/"
        xmlns:ns5=" http://eu.domibus.wsplugin/"
        xmlns:xmime="http://www.w3.org/2005/05/xmlmime">NOT_FOUND</ns5:getStatusResponse>
    </soap:Body>
</soap:Envelope>
----
====

====== UC04 List Pending Messages
.Click to Open
[%collapsible]
====
.UC04 List Pending Messages
[[uc4_ws]]

Brief description::
List the status Messages pending to be received by the Backend C4.
+
.Sequence Diagram C4 to C3 – ListPendingMessages
image:image8.png[width=226,height=99]

[caption=,title=Actors and Preconditions]
[width="100%",cols="10%,90%",options="header"]
|===
2+|*Actors*
|C4|Backend C4
2+|*Preconditions*
| |There is at least one message be downloaded by Backend C4.
| |The Receiving AP C3 is up and running and properly configured.
|===

.Basic Flow event
Steps:
1. Backend C4 launches the service listPendingMessages.
2. The Access Point (Receiving AP C3) retrieves the list of messageIds for messages with status RECEIVED.
3. Use case ends.

.Exception Flow
* N/A

[caption=,title=Post conditions]
[width="100%",cols="10%,90%"]
|===
h|*Successful conditions*
|The operation is a success if listPendingMessagesResponse
contains all the messageIDs of the pending messages to be retrieved or
the list is empty in the case that there are no pending messages.
h|*Failure Conditions*
|N/A
|===

.Special requirements
* N/A

.Security
In Multitenancy mode, the general property `domibus.auth.unsecureLoginAllowed`
is ignored and authentication is always required. +
For more about authentication options, see the Domibus Authentication section in the
<<adminguide, Administration>>. +
When authentication is required, the returned messages will always be
filtered having authenticated user overwriting finalRecipient value (if
finalRecipient is provided as below). +
The listPendingMessagesRequest accepts 8 _optional_ parameters in order
to filter the returned list of messages in status RECEIVED. +
Date time optional parameters like “receivedFrom” and “receivedTo” can
be provided in ISO-8601 format, with or without an offset. When the
offset is provided, the user MUST NOT provide any additional timezone
IDs so a value of “2021-07-21T14:27:00+02:00[Europe/Brussels]” is
invalid. When the offset is missing, these parameter values are
considered to be provided in UTC, having an offset of “+00:00”. For
example, the following two values are valid and point to the same
instant of 21^st^ of July 2021, 12:27:00 in UTC:

1. “2021-07-21T14:27:00+02:00” will be interpreted to have an offset of 02:00;
2. “2021-07-21T12:27:00” will be interpreted to be in the UTC timezone with a +00:00 offset.

.Request Example
[source, XML]
----
<soap:Envelope
    xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:_1="
    http://eu.domibus.wsplugin/">
    <soap:Header/>
    <soap:Body>
        <_1:listPendingMessagesRequest>
            <messageId>4078cfea-74e9-4058-9d14-1dceee597abd@domibus.eu</messageId>
            <conversationId>52f1c57d-bd35-4ab2-a0a5-da9a15101dba@domibus.eu</conversationId>
            <refToMessageId>d05051c6-951c-4f40-90b5-459eca9d8302@domibus.eu</refToMessageId>
            <fromPartyId>domibus-blue</fromPartyId>
            <finalRecipient>urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4</finalRecipient>
            <originalSender>urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1</originalSender>
            <receivedFrom>2021-01-15T09:00:00</receivedFrom>
            <receivedTo>2021-01-29T09:00:00</receivedTo>
        </_1:listPendingMessagesRequest>
    </soap:Body>
</soap:Envelope>
----

.Response Example
[source, XML]
----
<soap:Envelope
    xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
    <soap:Body>
        <ns6:listPendingMessagesResponse
            xmlns:S11="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:eb="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/"
            xmlns:S12="http://www.w3.org/2003/05/soap-envelope"
            xmlns:xmime="http://www.w3.org/2005/05/xmlmime" xmlns:ns6="
            http://eu.domibus.wsplugin/">
            <messageID>4078cfea-74e9-4058-9d14-1dceee597abd@domibus.eu</messageID>
        </ns6:listPendingMessagesResponse>
    </soap:Body>
</soap:Envelope>
----

====
