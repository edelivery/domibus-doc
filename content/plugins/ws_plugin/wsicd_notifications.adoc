[[wsplugin_notifications]]
==== Plugin Notifications

Domibus core notifies the WS Plugin on the following events:

* `MESSAGE_RECEIVED`
* `MESSAGE_SEND_FAILURE`
* `MESSAGE_RECEIVED_FAILURE`
* `MESSAGE_SEND_SUCCESS`
* `MESSAGE_STATUS_CHANGE`

The type of events received can be configured using the WS Plugin
property `wsplugin.messages.notifications`. You will find that property
in the file `ws-plugin.properties` under `\domibus\plugins\` in the
Domibus configuration folder.
For more information see the <<plugin_cookbook,Plugin Cookbook>>.
