== API Documentation

Domibus scans automatically the classpath inside the plugin's .jar file:

* The *filename convention* is `<pluginname>-domibusServlet.xml`, as in the example: `config/ws-plugin-domibusServlet.xml`.
* The *expected location* (in the Java project) is: `/src/main/resources/config/`,
** while in the plugin's .jar, this file is located at `/config/;
** this file should contain the package name used for all the REST APIs (Example: `com.custom.plugin.rest`):
+
[source, java]
----
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
         http://www.springframework.org/schema/beans
         http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
         http://www.springframework.org/schema/context
         http://www.springframework.org/schema/context/spring-context-3.0.xsd">
 <context:component-scan base-package="com.custom.plugin.rest" />
</beans>
----

.Example of a simple Rest API:
[source, java]
----
package com.custom.plugin.rest;
@RestController
 @RequestMapping(value = "/custom/plugin")
 public class CustomPluginConfiguration {
   @GetMapping(path = "/example")
    public ResponseEntity<String> get() {
     return ResponseEntity.ok("rest service triggered");
}
----
//first { missing

This API will be called with the path:
//something missing here

NOTE: Standard Javadoc documentation for the API can be downloaded from:
https://ec.europa.eu/[.line-through]#digital#-building-blocks/wikis/display/DIGITAL/Domibus+releases.
