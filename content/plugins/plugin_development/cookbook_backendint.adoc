== Backend Integration

=== General Overview

The purpose of Domibus is to facilitate B2B communication. To achieve
this goal it provides a very flexible plugin model which allows the
integration with nearly all back office applications.

There are three default plugins available with the Domibus distribution:

* the domibus-default-jms-plugin
* the domibus-default-ws-plugin
* the domibus-default-fs-plugin

[NOTE.seealso,caption=See Also]
====
Further documentation about those plugins can be found at
https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus.
====

IMPORTANT: Developers of custom plugins should ensure that
their plugins can be deployed alongside the standard Domibus Default
Plugins, given that they share a common classloader and Spring context.

=== Plugin Structure

A plugin is dependent on the `domibus-plugin-api` module which is
released together with the main Domibus application. Any changes to
previous API versions will be addressed in a migration guide.

In addition to this required module, another module is available (more
information about this module can be found in the *Domibus* Software
Architecture Document.

.Software Architecture Document
NOTE: Download the PDF at https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus in the documentation section.

* `domibus-logging`: may be used to maintain a uniform logging style
with the Domibus core.

A plugin consists of the implementation of:

* at least two interfaces
** `eu.domibus.plugin.transformer.MessageSubmissionTransformer`
** `eu.domibus.plugin.transformer.MessageRetrievalTransformer`
* and the extension of one abstract class,
** `eu.domibus.plugin.AbstractBackendConnector`

This way multiple plugins can share the same data formats while using
different transport protocols or enforcing different security policies.
It is also possible to implement transport handlers for protocols while
keeping the actual data format pluggable as those classes are not
necessarily coupled and can be reused independently from each other.

=== Message Flow

.Message Submission from the backend
image:image1.png[image,width=1413,height=576]

.Message reception by C3 from C2
image:image2.png[image,width=1454,height=516]
