[[plugins]]
= Plugins

//Default Plugins

//Nivel 2
include::plugin_deployment/default_plugins.adoc[]

//Nivel 3
include::fs_plugin/index_fs.adoc[]
include::ws_plugin/index_wsicd.adoc[]
include::old_wsplugin/old_wsplugin_icd.adoc[]
include::jms_plugin/index_jms.adoc[]

//Nivel 2
//Custom Plugins

include::plugin_deployment/custom_plugins.adoc[]

include::plugin_development/index_plugincookbook.adoc[]


