[[jms_interface]]
=== JMS Plugin Interface

The purpose of this content is to outline the JMS Data Format Exchange
to be used as part of the default JMS backend integration solution for
the Domibus Access Point
footnote:[https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus].

According to eDelivery, an Access Point is an implementation of the
OpenPEPPOL AS2 Profile or the eDelivery AS4 Profile. The data exchange
protocols of eDelivery are profiles, meaning that several options of the
original technical specifications were narrowed down in order to
increase consistency, interoperability and to simplify deployment. The
profile of AS2 was developed by
OpenPEPPOLfootnote:[http://www.peppol.eu/], and the profile of AS4 was
developed by e-SENSfootnote:[http://www.esens.eu/] in collaboration with
several service providers while being implemented in the e-Justice
domain by e-CODEX. An Access Point exposes two interfaces:

* An interface to connect the Backend system with the Access Point.
Typically, this interface is customisable as communication between
Access Points and Backend systems may use any messaging or transport
protocol.
* A standard messaging interface between Access Points, this interface
is configurable according to the options of the profiles supported by
eDelivery. It is important to note that eDelivery standardises the
communication only between the Access Points.

This document will univocally define the JMS plugin that acts as an
interface to the Access Point (Corner Two and Corner Three in the four
corner topology that will be explained later in this document) component
of the eDelivery building block.

There is 1 interface described in this document:

.Table 1 - Interface described
[width="100%",cols="46%,27%,27%",options="header"]
|===
|Interface |Description |Version
|JMS backend integration |The JMS plugin |4.x.y
|===

.Scope
This document covers the service interface of the Access Point from the
perspective of the JMS backend integration. It includes information
regarding the description of the JMS-Queues, information model and the
types of messages for the services provided. This specification
addresses no more than the service interface of the Access Point. All
other aspects of its implementation are not covered by this document
(i.e. the service consumer). The ICD specification provides both the
provider (i.e. the implementer) of the services and their consumers with
a complete specification of the following aspects:

* Interface Functional Specification, this specifies the set of services
and the operations provided by each service;
* Interface Behavioural Specification, this specifies the expected
sequence of steps to be respected when calling a service or a set of
services;
* Interface Message standards, this specifies the syntax and semantics
of the data and metadata;
* Interface Policy Specification, this specifies constraints and
policies regarding the operation of the service.

.Audience
This document is intended to the Directorate Generals and Services of
the European Commission, Member States (MS) and also companies of the
private sector wanting to set up a connection between their backend
systems and the Access Point.

In particular:

* Architects will find it useful for determining how to best exploit the
Access Point to create a fully-fledged solution and as a starting point
for connecting a Back-Office system to the Access Point.
* Analysts will find it useful to understand the Access Point that will
enable them to have a holistic and detailed view of the operations and
data involved in the use cases.
* Developers will find it essential as a basis of their development
concerning the Access Point plugin services.
* Testers can use this document in order to test the interface by
following the use cases described.

.Acronyms
.Table 3 - domibus.backend.jmsInQueue messagefields
[width="100%",cols="15%,85%",options="header"]
|===
|*Acronym* |*Definition*
|ebMS# |ebXML Messaging Service Specification

|MEP# a|
Message Exchange Pattern

A Message Exchange Pattern describes the pattern of messages required by
a communications protocol to establish or use a communication channel.

|ebXML# a|
Electronic Business XML

Project to use XML to standardise the secure exchange of business data.

|P-Mode# |Processing Mode

|MSH a|
Message Service Handler

The MSH is an entity that is able to generate or process messages that
conform to the ebMS specification, and which act in at least one of the
two ebMS roles: Sender and Receiver.

In terms of SOAP processing, an MSH is either a SOAP processor or a
chain of SOAP processors. In either case, an MSH has to be able to
understand the eb:Messaging header (qualified with the ebMS namespace).

|===

include::jmsicd/jmsicd_funcspec.adoc[leveloffset=+2]
include::jmsicd/jmsicd_behavspec.adoc[leveloffset=+2]
include::jmsicd/jmsicd_notifications.adoc[leveloffset=+2]
include::jmsicd/jmsicd_multitenancy.adoc[leveloffset=+2]