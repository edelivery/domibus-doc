[[jms_funcspec]]
== Functional Specification

=== The four corner model

In order to understand the Use Cases that will be described below it is
important to explain the topology; i.e. the four – corner model.

image:image1.png[width=528,height=216]

.Figure 1 - The four corner model

In this model we have the following elements:

* Corner One (C1): Backend C1 is the system that will send messages to
the sending AP (Access Point)
* Corner Two (C2): Sending Access Point C2
* Corner Three (C3): Receiving Access Point C3
* Corner Four (C4): Backend C4 is the system that will receive messages
from the receiving AP (Access Point)

The JMS backend is described in this document. JMS (Java Message
Service) is an API that provides the facility to create, send and read
messages. It provides loosely coupled, reliable and asynchronous
communication. JMS is also known as the standard for Java asynchronous
messaging service. Messaging is a technique to enabling
inter-application communications.

There are two types of messaging domains in JMS.

* Point-to-Point Messaging Domain
* Publisher/Subscriber Messaging Domain

The present JMS backend integration uses
https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern[*Publisher/Subscriber Messaging pattern*] where senders of messages, called publishers, do not plan the messages to be sent directly to specific receivers (called
subscribers) but, instead, characterize published messages into classes
without knowledge of which subscribers will be. Similarly, subscribers
express interest in one or more classes and only receive the messages
that are of their interest, without knowledge of which publishers are
sending those messages. The intent of interest is done by means of a
subscription.

=== Introduction to Domibus - AS4

Using as reference https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus[DEP
DIGITAL],
Domibus is the Open Source project of the
http://www.e-codex.eu/about-the-project/technical-solution/domibus-software.html[AS4
Access Point] maintained by the European Commission. Third-party
software vendors offer alternative implementations of the eDelivery AS4
Profile (commercial or open-source). Each software vendor also provides
different added-value services from integration to the support of
day-to-day operations. For safeguarding interoperability, eDelivery
encourages implementers to consult the list of software products that
have passed the conformance tests by the European Commission of the
https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/eDelivery+AS4+conformant+solutions
[eDelivery AS4
profile].

The sample software, Domibus, may be used to test other implementations
of the AS4 profile or as a working solution in a production environment.
The users of the sample implementation remain fully responsible for its
integration with backend systems, deployment and operation. The support
and maintenance of the sample implementation, as well as any other
auxiliary services, are provided by the European Commission according to
the terms defined in the eDelivery Access Point Component Offering
Description.

It is also important to comment on the PMode. A processing mode – or
PMode – is a collection of parameters that determine how user messages
are exchanged between a pair of Access Points with respect to Quality of
Service, Transmission Mode and Error Handling. A PMode maps the
recipient Access Point from the partyId, which represents the backend
offices associated to this Access Point.
