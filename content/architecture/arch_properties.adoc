==== Domibus Properties

The Domibus properties defined in the `domibus.properties` file are used
for the _single_ domain, when Domibus manages one single domain.

When Domibus is configured with multiple domains, several properties
will have to be customized per domain. More information on which
properties can be overridden per domain are available in the
<<adminguide, Administration Guide>>.

There are three types of properties in Multitenancy mode:

* *Global/Infrastructure* properties that have a meaning for the whole
application and, as such, a single value

* *Domain* properties that can/should be customized for each domain. In
case a value is not defined for a domain, it falls back to the one
defined in global properties file (or an error is thrown in case the
fallback is not allowed by the property metadata)

* *Super* properties applicable to super-users. Same behaviour as domain
properties.

To distinguish between these types of properties, one needs to define a
property _metadata_ in the appropriate _metadata manager_ (each module
has its own property metadata manager). If a property does not have a
metadata defined, then it is treated as global.

In order to define a property, the following conventions are used:

[width="100%",cols="25%,36%,39%",options="header"]
|===
|Property Type |File where it is defined |Property name
|Global/Infrastructure |*domibus.properties* |domibus.config.location

|Domain property, +
domain1 |*domain1-domibus.properties*
|*domain1*.domibus.security.keystore.location

|Domain property, +
default domain |*default-domibus.properties*
|*default*.domibus.security.keystore.location

|Super property |*super-domibus.properties*
|*super*.domibus.console.login.maximum.attempt
|===

All common properties are defined in *core metadata manager*. The
properties that are specific to a server (Tomcat, WildFly, WebLogic and
WebLogic-ecas) are defined in a *specific metadata manager* like
TomcatMetadataManager. All these properties are managed by the Domibus
Property Provider.

These are considered as *internal properties,* as opposed to properties
defined in *external modules* and *plugins* (DSS module, JmsPlugin).
Each external module has also *a property manager* that gets and sets
its own properties.

External modules can manage their properties by using their own property
bag (as do default WS and FS plugins) or by delegating to the Domibus
Property Provider bag (as do default JMS plugin and DSS module).

All property metadata, both internal and external, are managed centrally
by the Domibus Property Provider.

The property metadata is used by the Domibus Property Provider to
determine how and from where to get and set its value.

Domibus Property Provider is the single-entry point for getting and
setting property values in MSH.

An external module can use its own property manager to do the same or it
can call the Domibus Property Provider Delegate to do the same (as it
manages all properties and knows how to route the call to the
appropriate module manager).

