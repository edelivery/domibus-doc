[[architecture_multitenancy]]
=== Multitenancy

There were multiple options to choose from to support Multitenancy:

* *One Schema per tenant*: tenant's data is saved in
the same database for all tenants but in different schemas. When a new
tenant needs to be added a new related DB schema is created in the same
database instance. It is easier to add new tenants comparing with the DB per tenant as the same connection pool can be reused.
Switching between tenants is performed centrally by selecting the DB
schema related to the tenant. A huge advantage of this approach is that
the application code impact is limited compared to the _Discriminator
field_ approach described below.

* *One DB per tenant*: each tenant has its own separate database. This
is the highest level of isolation; however, it is complex and cumbersome
to maintain. Whenever a new tenant must be added a new database instance
needs to be created, a new database connection pool also needs to be
created in Domibus which points to the tenant database, etc.

* *Discriminator field*: All tenants' data is saved on common tables,
and each table holds a discriminator field to distinguish data from each
tenant. This approach has quite some disadvantages: no physical
isolation of data between tenants (a bug in the application might leak
between tenants), performance decrease as the data for all the tenants
are saved into the same tables (resulting in bigger tables and more
complex/heavier queries), and significant changes to the application
code to take into account that discriminator field.

==== Multitenancy Approach

Domibus implements the *One Schema per tenant"* solution for Multitenancy support
because of its many advantages. The Hibernate library, which is used in Domibus, comes with support by default for the *One Schema per tenant* strategy.

//broken link
[NOTE.seealso,caption=See Also]
====
For more info about *Multitenancy in Hibernate*, see
https://docs.jboss.org/hibernate/orm/.0/userguide/html_single/chapters/multitenancy/MultiTenancy.html
====

