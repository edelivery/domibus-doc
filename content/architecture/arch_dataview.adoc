=== Data View

==== Data Model

The following diagrams show a high-level abstraction of the data
entities, which must be implemented by the system:

image:image7.png[width=582,height=363]

The above tables represent a mapping of the ebMS3 XSD to database
tables.

`TB_USER_MESSAGE`:: table containing information about the AS4 UserMessage
metadata (both sent and received ones).

`TB_USER_MESSAGE_LOG`:: table containing runtime information about the
User Message (both sent and received ones) such the message status,
retry information, etc.

`TB_USER_SIGNAL_MESSAGE`:: table containing information about the AS4
SignalMessage metadata (both sent and received ones).

`TB_SIGNAL_MESSAGE_LOG`:: table containing runtime information about the
Signal Messages(acknowledgements) for sent User Messages.

image:image9.png[width=986,height=430]

The above tables represent a 1:1 mapping of the PMode configuration XSD
to database tables.

image:image10.png[]

Routing criteria contains the data that are needed to perform the
routing of the messages to a specific plugin implementation.

Backend filters are collections of routing criteria associated with a
specific backend representation.

image:image11.png[Diagram Description automatically
generated,width=582,height=323]

The above tables are used by the Quartz library which is used by Domibus
for crontab like jobs.

===== Data Auditing

The database tables contain information which provide audit information
about their entries including the name of the user who created or last
updated a particular entry or about the creation time or the last update
time of a particular entry. The stored values are the following:

`CREATED_BY`, `MODIFIED_BY`, `CREATION_TIME`, `MODIFICATION_TIME`

These values are not visible to the Domibus user and can be only
inspected at the database level. Data created or updated by the Domibus
users will use their usernames for the `CREATED_BY` and `MODIFIED_BY`
columns. Data created using plugin users will use their plugin usernames
for the `CREATED_BY` and `MODIFIED_BY` columns. The rest of the entries
(e.g., created during the invocation of an asynchronous JMS listener)
will use the username used to connect to the database for MySQL or the
Oracle user schema for the `CREATED_BY` and `MODIFIED_BY` columns. +
There is one exception involving reference data (i.e., data that
has to exist before the application can be used) on MySQL where we
use the value `DOMIBUS` for the `CREATED_BY` and `MODIFIED_BY` columns.

==== State Machines

[discrete]
===== Outgoing Message State Machine

The outgoing messages have the following state machine:

image:image12.png[Outgoing Message State Machine]

[discrete]
===== Incoming Message State Machine

The incoming messages have the following state machine:

image:image13.png[Incoming Message State Machine]

[discrete]
==== Pull Messages State Machine

image:image16.png[width=702,height=248]

