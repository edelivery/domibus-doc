==== User to Domain Association

When a user authenticates in the Admin Console, the domain is not yet
identified and Domibus must find out which DB schema to select. In order
to achieve this, a *general DB schema* is used. In this general DB
schema, a table tells which user that has access to the Admin Console
(defined in the `tb_user` table) and to the domain he belongs to. This
association is automatically updated by Domibus when users are added or
removed. Therefore, a constraint has been added in Domibus: a username
needs to be unique amongst the existing domains. The same mechanism and
constraints apply to the `tb_user` table and has been implemented for
the table supporting plugins security: `tb_authentication_entry`.

