:imagesdir: images/architecture/

[[domibus_architecture]]
== Domibus Architecture

Domibus Access Point is a compliant implementation of the https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/eDelivery+AS4+conformant+solutions[eDelivery profile]
of the OASIS ebMS3/AS4 standard.

This content provides an overview and description of the most significant decisions underlying its current architecture on different levels: Use Case, Logical, Process, Deployment, Implementation, Data.

We also provide some considerations regarding Sizing, Performance and Quality.

NOTE: It's not our goal with this content to explain the ebMS3/AS4 standards,
the four-corner model or any other concepts described in the provided references.
For more about this see http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/profiles/AS4-profile/v1.0/[OASIS AS4 Profile].

//Includes
:imagesdir: content/_common/images/architecture/

include::arch_overview.adoc[]
include::arch_usecase.adoc[]
include::arch_logic.adoc[]
include::arch_deployment.adoc[]
include::arch_implementation.adoc[]
include::arch_dataview.adoc[]
include::arch_sizeperformance.adoc[]
include::arch_logging.adoc[]
include::arch_multitenancy_logging.adoc[]
include::arch_caching.adoc[]
include::arch_multitenancy.adoc[]
include::arch_multitenancy_domainid.adoc[]
include::arch_multitenancy_userdomain.adoc[]
include::arch_multitenancy_plugins.adoc[]
include::arch_properties.adoc[]
include::arch_message_payloads.adoc[]
include::arch_multitenancy_quartz.adoc[]
include::arch_payloadSPI.adoc[]
include::arch_configurationSPI.adoc[]
