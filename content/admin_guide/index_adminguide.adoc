[[adminguide]]
== Administration Guide
The Administration Guide is a collection of sections on how to install and configure Domibus:

* <<domibus_installation>>
* <<domibus_config>>
* <<admintools>>
* <<opsguides>>
* <<domibus_upgrade>>

//includes
include::installation/index_installation.adoc[]
include::configuration/index_configuration.adoc[]
//
//Upgrade Section
[[domibus_upgrade]]
== Upgrading Domibus

See <<domibus_upgradeguides, Upgrading Guides>>.


//
include::admin_console/index_adminconsole.adoc[]
include::ops_guides/index_opsguides.adoc[]
