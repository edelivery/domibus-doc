:imagesdir: content/_common/images/logginglevels/

== Setting Logging levels at runtime

=== Description

Admin and Super admin users can change the Logging levels at runtime for
the Domibus application using the Admin Console *Logging* menu:

image:image12.png[image,width=623,height=316]

.Input elements include:
* A *Search box* where the user could freely enter the name of the
package of classes desired to set the logging level. By default this is
populated with ‘eu.domibus’ value.

NOTE: Wildcards are not accepted like ‘domi*’ are not recognised.
Users must enter the full description of the item to be searched
(e.g:‘domibus’ or ‘apache’)

* A *Show classes* check box allows level setting for each package. See
the next picture
* A *Reset button* will reset all logging levels to the default values
defined in logback.xml
* *Pagination* controls to change the number of rows to be shown per
page

[NOTE]
====
* In a multi-tenant environment, loggers' names are prefixed
with the tenant's name to which the selected logging level is to be applied.
+
For example, if the server has two tenants named _tenantOne_
and _tenantTwo_, you can configure these loggers separately by naming them as:
+
`tenantOne.eu.domibus` +
`tenantTwo.eu.domibus`

* Changing the logging levels only affects the currently running
instance of Domibus and will not change or update the existing logging
configuration file (`logback.xml`).
====

image:image13.png[image,width=623,height=332]
