== Payload Encryption

Data at rest is not encrypted by default in Domibus. This means that the
payloads are stored in C2 exactly as they were received from C1. The
same for payloads received from C2 and stored in C3.

The payloads stored in C2 and C3 are not accessible to third parties.
Nevertheless, it is a good practice to encrypt the payloads to
increase the security level.

Data at rest encryption can be activated using the property
`domibus.payload.encryption.active=true`. Once activated, Domibus
encrypts the payloads stored in C2 and C3 using symmetric encryption
with AES/GCM/NoPadding algorithm. Domibus generates the symmetric key
used to encrypt payloads the first time the payload encryption is
activated. The generated symmetric key is stored in the Domibus
database. A symmetric key is generated for each domain in case of
multitenancy.

Encrypting data at rest is transparent for C1/C4, so if C4 downloads a
message from C3, it will receive the payloads un-encrypted as they were
sent by C1.
