[[message_pulling]]
== Message pulling

=== Setup

In order to configure message pulling, the process section should be
configured with `mep` set to `oneway` and binding set to `pull` as shown
in the following example:


[source, xml]
----
<process name="tc1Process"
    agreement="agreementEmpty"
    mep="oneway"
    binding="pull"
    initiatorRole="defaultInitiatorRole"
    responderRole="defaultResponderRole">
    <initiatorParties>
        <initiatorParty name="initiatoralias"/>
    </initiatorParties >
    <responderParties>
        <responderParty name="receiveralias"/>
    </responderParties>
    <!-- no initiatorParties element -->
    <legs>
        <leg name="pushTestcase1tc1Action"/>
    </legs>
</process>
----

In the case of a pull process, the `initiatorParties` section contains
the party that initiate the pull request. The `responderParties` section
contains the parties that can be pulled from.

In `domibus.properties` configuration file, adapt the following properties
to your needs. Note that domibus.msh.pull.cron and `domibus.pull.queue.concurrency` are mandatory.

IMPORTANT: The process handling the pull mechanism is _not_ concurrent. +
If a large payload is processed, a subsequent pull request is only executed once the first execution is completed. Define the size of `domibus.pull.queue.concurrency` having this into account.

[source, properties]
----
#Cron expression used for configuring the message puller scheduling.
#domibus.msh.pull.cron=0 0 0/1 * * ?

# Number of threads used to parallelize the pull requests.
#domibus.pull.queue.concurency=1-1

# Number of threads used to parallelize the pull receipts.
#domibus.pull.receipt.queue.concurrency=1-1

#Number or requests executed every cron cycle
#domibus.pull.request.send.per.job.cycle=1

#Time in second for the system to recover its full pull capacity when
job schedule is one execution per second.
#If configured to 0, no incremental frequency is executed and the pull
pace is executed at its maximum.
#domibus.pull.request.frequency.recovery.time=0

#Number of connection failure before the system decrease the pull
pace.
#domibus.pull.request.frequency.error.count=10

#Pull Retry Worker execution interval as a cron expression
#domibus.pull.retry.cron=0/10 * * * * ?
----

If high frequency pulling is used (job configured every second), it is
possible to configure the system to lower the pulling frequency in case
the counterpart access point is unavailable. Per default if the other
access point returns errors 10 times in a row
(domibus.pull.request.frequency.error.count) the number of pull requests
per job cycle will fall to 1 per mpc. As from the moment, the
counterpart access point is responding again, Domibus will take the
amount of seconds configured within the
domibus.pull.request.frequency.recovery.time property to recover the
pulling pace configured within the
`domibus.pull.request.send.per.job.cycle property`.

Per default, domibus.pull.request.frequency.recovery.time=0 which means
that the throttling mechanism is off.

NOTE: An access point is considered unavailable if it returns the
EBMS error code: `EBMS_0005`.

The following properties are used for dynamic pulling and are
recommended to be used only with a custom authorization extension:

----

#Allow dynamic initiator on pull requests - 0 or multiple initiators are
allowed in the PMode process
#domibus.pull.dynamic.initiator=false

#Allow multiple legs configured on the same pull process (with the same
security policy)
#domibus.pull.multiple_legs=false

#Force message into READY_TO_PULL when mpc attribute is present
#domibus.pull.force_by_mpc=true

#Mpc initiator separator. This is used when the mpc provides information
on the initiator: baseMpc/SEPARATOR/partyName

#domibus.pull.mpc_initiator_separator=PID

----

=== Configuration restriction

A correctly configured *one-way pull process* should only contain one
party configured in the *initiatorParties* section.

Different *legConfiguration* with the same *defaultMpc* (highlighted in
red in the following configuration) should not be configured in the same
pull process or across different pull processes.

If those restrictions are not respected, the message will not be
exchanged and a warning message will detail the configuration problem.

[source, xml]
----
<legConfiguration name="pushTestcase1tc2Action"
    service="testService1"
    action="tc2Action"
    defaultMpc="*defaultMpc*"
    reliability="AS4Reliability"
    security="eDeliveryAs4Policy"
    receptionAwareness="receptionAwareness"
    propertySet="eDeliveryPropertySet"
    payloadProfile="MessageProfile"
    errorHandling="demoErrorHandling"
    compressPayloads="true"/>
----
