:imagesdir: ../../images/pmode/

[[config_pmode]]
=== PMode Configuration

Processing Modes (PModes) are used to configure Access Points.
The PMode parameters are loaded into the Access Point via an XML file.

The features described in the PMode file are:

////
[width="100%",cols="^.^1,^.^1,^.^1", grid="none"]
|===
h|Security h|Reliability h|Transport
h|Business Collaborations h|Error Reporting h|Message Exchange
h|Patterns (MEPs) h|Message Partition Channels (MPCs) h|
|===
////

* Security
* Reliability
* Transport
* Business Collaborations
* Error Reporting
* Message Exchange
* Patterns (MEPs)
* Message Partition Channels (MPCs)

As different messages may be subject to various types of processing or, as different business domains may have several requirements, Access Points commonly support several PModes.

[NOTE.seealso,caption=See Also]
====
For more information, please refer to the https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/Access+Point+software?preview=%2F23003387%2F26804344%2F%28CEFeDelivery%29.%28AccessPoint%29.%28COD%29.%28v1.04%29.docx[Access Point Component Offering Document].
====

==== PMode Configuration Files

In Domibus, PModes are XML files where network participants are represented and configured via the `party` element in configuration files.

You can configure the provided files:

* `<edelivery_path>/conf/pmodes/domibus-gw-sample-pmode-party_id_name1.xml`
* `<edelivery_path>/conf/pmodes/domibus-gw-sample-pmode-party_id_name2.xml`

_Where:_

* `<edelivery_path>` stands for directory where you have have Domibus installed.
* `party_id_name1` and `party_id_name2`, are placeholders representing names for existing parties.

////
REVIEW QUESTIONS: It is not clear what about the files above, because they have sample in the name as well is part
of an example or actually exist in the installation and need to be modified. Or will not be used at all by Domibus
and you should define your own (at this point in the section, right off the bat:) I think it should be clearer
1. describe the target folder where Domibus expects to find these files,
2. their necessary naming convention
3. minimum description in sample, an example per each each pmode config extra need
////

IMPORTANT: The corresponding `partyId` attribute's value needs to match the certificate's alias in the keystore
and the endpoint must be the access link external to your instance. +
This step can be managed by a PMode Configuration Manager, known to your Business Owner.

[source, XML]
----
<party name="party_id_name2" endpoint="http://party_id_name2_hostname:8080/domibus/services/msh">
    <identifier partyId="party_id_name2_1" partyIdType="partyTypeUrn"/>
</party>
<party name="party_id_name1" endpoint="http://party_id_name1_hostname:8080/domibus/services/msh">
    <identifier partyId="party_id_name1_1" partyIdType="partyTypeUrn"/>
</party>
----
//Questions
//relationship between party.name and identified.partyId, in the example party_id_name2 vs party_id_name2_1
//is there a particular reason the order is AP2, AP1?

===== Adding a New Participant

If a new Access Point(AP) participant is joining your network, you need to modify the PMode file accordingly
and re-upload it as described in <<pmode_uploadnew>>.

.Adding a new `party` element
[source, XML]
----
<party name="new_party_name" endpoint="http://new_party_msh" >
    <identifier partyId="new_party_id" partyIdType="partyTypeUrn"/>
</party>
----

* Participants with `initiator` role are message senders. See a sample below:

.Defining initiator parties (senders)
[source, xml]
----
<initiatorParties>
<!-- ... -->
    <initiatorParty name="<new_party_name>"/>
</initiatorParties>
----

* Participants with `responder` role are message receivers. See a sample below:

.Defining responder parties (receivers)
[source, xml]
----
<responderParties>
…
<responderParty name="new_party_name"/>
</responderParties>
----

==== Sample Processing Modes (PModes) Files

Processing Modes (PModes or PMode) describe how messages are exchanged between AS4 partners. PMode files contain the
identifiers of each AS4 Access Point. +
Sender and Receiver identifiers represent the organizations that send and receive the business documents. +
Senders and receivers are part of the authorization process (PMode), this means adding, modifying or
deleting a participant involves modifying the corresponding PMode files.

PMode files are XML files where the network's Access Points, identified as `parties` are listed and how they are to exchange messages is described. See an example below.

.Sample PMode file
[%collapsible]
====
.Sample PMode file
[source, xml]
----
<?xml version="1.0" encoding="UTF-8"?>

<db:configuration xmlns:db="http://domibus.eu/configuration" party="blue_gw">

<mpcs>
    <mpc name="defaultMpc"
         qualifiedName="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC"
         enabled="true"
         default="true"
         retention_downloaded="0"
         retention_undownloaded="14400"
         retention_sent_success="14400" <1>
         retention_sent_failure="14400" <2>
         retention_sent="14400"
         delete_message_metadata="false"
         max_batch_delete="1000"/>
</mpcs>
<businessProcesses>
    <roles>
        <role name="defaultInitiatorRole"
              value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator"/>
        <role name="defaultResponderRole"
              value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder"/>
</roles>
<parties>
    <partyIdTypes>
        <partyIdType name="partyTypeUrn"
                     value="urn:oasis:names:tc:ebcore:partyid-type:unregistered"/>
        </partyIdTypes>
<party name="red_gw" endpoint="http://red_hostname:8080/domibus/services/msh">
    <identifier partyId="domibus-red" partyIdType="partyTypeUrn"/>
</party>
<party name="blue_gw" endpoint="http://blue_hostname:8080/domibus/services/msh">
    <identifier partyId="domibus-blue" partyIdType="partyTypeUrn"/>
</party>
</parties>
<meps>

<mep name="oneway"
value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/oneWay"/>

<mep name="twoway"
value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/twoWay"/>

<binding name="push"
value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/push"/>

<binding name="pull"
value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/pull"/>

<binding name="pushAndPush"
value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/push-and-push"/>
</meps>

<properties>
    <property name="originalSenderProperty"
        key="originalSender"
        datatype="string"
        required="true"/>
    <property name="finalRecipientProperty"
        key="finalRecipient"
        datatype="string"
        required="true"/>
   <propertySet name="eDeliveryPropertySet">
       <propertyRef property="finalRecipientProperty"/>
       <propertyRef property="originalSenderProperty"/>
   </propertySet>
</properties>

<payloadProfiles>
    <payload name="businessContentPayload"
        cid="cid:message"
        required="true"
        mimeType="text/xml"/>
    <payload name="businessContentAttachment"
        cid="cid:attachment"
        required="false"
        mimeType="application/octet-stream"/>

<payloadProfile name="MessageProfile" maxSize="2147483647">
    <attachment name="businessContentPayload"/>
    <attachment name="businessContentAttachment"/>
</payloadProfile>
</payloadProfiles>

<securities>
 <security name="eDeliveryAS4Policy"
     policy="eDeliveryAS4Policy.xml"
     signatureMethod="RSA_SHA256"/>
</securities>

<errorHandlings>
    <errorHandling name="demoErrorHandling"
        errorAsResponse="true"
        businessErrorNotifyProducer="true"
        businessErrorNotifyConsumer="true"
        deliveryFailureNotifyProducer="true"/>
</errorHandlings>

<agreements>
    <agreement name="agreement1" value="A1" type="T1"/>
</agreements>

<services>
 <service name="testService1" value="bdx:noprocess" type="tc1"/>
 <service name="testService"
    value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/service"/>
</services>

<actions>
 <action name="tc1Action" value="TC1Leg1"/>
 <action name="testAction"
 value="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/test"/>
</actions>

<as4>
 <receptionAwareness name="receptionAwareness" retry="12;4;CONSTANT" duplicateDetection="true"/>
 <reliability name="AS4Reliability" nonRepudiation="true" replyPattern="response"/>
</as4>

<legConfigurations>
 <legConfiguration name="pushTestcase1tc1Action"
     service="testService1"
     action="tc1Action"
     defaultMpc="defaultMpc"
     reliability="AS4Reliability"
     security="eDeliveryAS4Policy"
     receptionAwareness="receptionAwareness"
     propertySet="eDeliveryPropertySet"
     payloadProfile="MessageProfile"
     errorHandling="demoErrorHandling"
     compressPayloads="true"/>

 <legConfiguration name="testServiceCase"
     service="testService"
     action="testAction"
     defaultMpc="defaultMpc"
     reliability="AS4Reliability"
     security="eDeliveryAS4Policy"
     receptionAwareness="receptionAwareness"
     propertySet="eDeliveryPropertySet"
     payloadProfile="MessageProfile"
     errorHandling="demoErrorHandling"
     compressPayloads="true"/>
 </legConfigurations>

 <process name="tc1Process"
     mep="oneway"
     binding="push"
     initiatorRole="defaultInitiatorRole"
     responderRole="defaultResponderRole">

 <initiatorParties>
  <initiatorParty name="blue_gw"/>
  <initiatorParty name="red_gw"/>
 </initiatorParties>

 <responderParties>
  <responderParty name="blue_gw"/>
  <responderParty name="red_gw"/>
 </responderParties>

 <legs>
  <leg name="pushTestcase1tc1Action"/>
  <leg name="testServiceCase"/>
 </legs>
</process>
</businessProcesses>

</db:configuration>
----

.Sample scenario
In the example above:

* we have two parties (AP's), `blue_gw` or `red_gw`;
* both allowed to initiate the process.

.Alternative scenario
To have only `blue_gw` sending messages, we would have:

* only `blue_gw` listed under `<initiatorParties>` and
* `red_gw` listed under `<responderParties>`.
====

==== Domibus versus ebMS3

In this section you can find information regarding how PMode configuration is done in Domibus and its counterparts in ebMS3 PMode configuration.

See the table below:

.Domibus PMode configuration to ebMS3 mapping
[width="100%",cols="50%,50%"]
|===
h|Domibus PMode Configuration / +
EbMS3 Specification [ebMS3CORE] [AS4-Profile]
h|Description
// End of header row
a| a|
// Separator row
2+h|Message Partition Channels (MPCs)
|
a|Container which defines the different MPCs.

//
2+h|Message Partition Channel (MPC)
a|`PMode[1].BusinessInfo.MPC:` +
The value of this parameter is the identifier of the MPC (Message Partition Channel) to which the message is assigned. +
It maps to the attribute `Messaging/UserMessage`.
a|MPC allows the partition of the flow of messages from a `Sending MSH` to a `Receiving MSH` into several flows that can be controlled separately. +
An MPC also allows merging flows from several `Sending MSHs` into a unique flow that can be treated as such by a `Receiving MSH`. +
The value of this parameter is the identifier of the MPC to which the message is assigned.

An MPC consists of following properties:

* `name`: identifier (required)
* `retention_downloaded`: Retention interval for messages already delivered to the backend.
* `retention_undownloaded`: Retention interval for messages not yet delivered to the backend.
* `retention_metadata_offset`: number of minutes to keep the metadata after the payload is deleted.
* `default`: [not implemented]
* `enabled`: [not implemented]
* `qualifiedName`: namespace-qualified name
* `retention_sent`: Retention interval for messages already sent with success or failed to the other MSH.
* `retention_sent_success`: Retention interval for messages already sent with success to the other MSH.
* `retention_sent_failure`: Retention interval for messages already sent with failed to the other MSH.
* `delete_message_metadata`: When true, message metadata is deleted together with the payload.
* `max_batch_delete`: Sets the maximum batch to be used when deleting messages in bulk. +
When there are multiple expired messages, they will be deleted in batches until all consumed.
//


2+h|Parties
a| -
a|Container which defines the different `PartyIdTypes`, `Party` and `Endpoint`.

//
2+h|PartyIdTypes
a|maps to the attribute `Messaging/UserMessage/` and `PartyInfo`
a|Message Unit bundling happens when the Messaging element contains multiple child elements or Units (either User Message Units or Signal Message Units).

//
2+h|Party ID
a|maps to the element `Messaging/UserMessage/` and `PartyInfo`
a|The ebCore Party ID type can simply be used as an identifier format and therefore as a convention for values to be used in configuration and – as such – does not require any specific solution building block.

//
2+h|Endpoint
a|maps to `PMode[1].Protocol.Address`
a|The endpoint is a party attribute that contains the link to the MSH. The value of this parameter represents the address (endpoint URL) of the `Receiver MSH` (or `Receiver Party`) to which Messages under this PMode leg are to be sent. Note that a URL generally determines the transport protocol (e.g. if the endpoint is an email address, then the transport protocol must be SMTP; if the address scheme is `HTTP`, then the transport protocol must be HTTP).

//
2+h|AS4 a|- a|Container.

//
2+h|Reliability +
[@Nonrepudiation] [@ReplyPattern]

a|
* `Nonrepudiation` maps to `PMode[1].Security.SendReceipt.NonRepudiation`
* `ReplyPattern` maps to `PMode[1].Security.SendReceipt.ReplyPattern`

a|
* `PMode[1].Security.SendReceipt.NonRepudiation`: value = 'true' or 'false'
** set as `TRUE` for non-repudiation of receipt
** set as `FALSE` for simple reception awareness

* `PMode[1].Security.SendReceipt.ReplyPattern: value = ‘Response´or 'Callback’`
** Set as Response to send receipts on the HTTP response or back-channel
** Set as Response to send receipts using a separate connection

2+h|ReceptionAwareness +
[@retryTimeout] [@retryCount] [@strategy] [@duplicateDetection]

a|
* `retryTimeout` maps to +
`PMode[1].ReceptionAwareness.Retry=true` +
`PMode[1].ReceptionAwareness.Retry.Parameters`
* `retryCount` maps to +
`PMode[1].ReceptionAwareness.Retry.Parameters`
* `strategy` maps to +
`PMode[1].ReceptionAwareness.Retry.Parameters`
* `duplicateDetection` maps to +
`PMode[1].ReceptionAwareness.DuplicateDetection`

a|These parameters are stored in a composite string.

* `retryTimeout` defines timeout in minutes.
* `retryCount` is the total number of retries.
* `strategy` defines the frequency of retries. Strategy available:
1. `CONSTANT`: the time between retries is constant.
2. `SEND_ONCE`: no retry.
3. `PROGRESSIVE`: the time between retries increase gradually.
* `duplicateDetection` allows to check duplicates when receiving twice the same message. +
The only `duplicateDetection` available as of now is `TRUE`.

//
2+h|Securities a|- a|Container
//
2+h|Security a|- a|Container
//
2+h|Policy
a|
`PMode[1].Security. NOT including` +
`PMode[1].Security.X509.Signature.Algorithm`
a|The parameter defines the name of a WS-SecurityPolicy file.

//
2+h|SignatureMethod
a|`PMode[1].Security.X509.Signature.Algorithm`
a|This parameter is not supported by WS-SecurityPolicy and therefore it is defined separately.

//
2+h|BusinessProcessConfiguration a|- a|Container

//
2+h|Agreements
a|maps to `eb:Messaging/UserMessage/` `CollaborationInfo/AgreementRef`
a|This optional element occurs zero times or once. The `AgreementRef` element is a string that identifies the entity or artifact governing the exchange of messages between the parties.

//
2+h|Actions a|- a|Container.

//
2+h|Action
a|maps to `Messaging/UserMessage/CollaborationInfo/Action`
a|This required element occurs once. The element is a string identifying an operation or an activity within a Service that may support several of these.

//
2+h|Services a|- a|Container

//
2+h|ServiceTypes Type
a|maps to `Messaging/UserMessage/CollaborationInfo/Service[@type]`
a|This required element occurs once. It is a string identifying the service that acts on the message and it is specified by the designer of the service.

//
2+h|MEP +
[@Legs]
a|-
a|An ebMS MEP defines a typical choreography of ebMS User Messages which are all related through the use of the referencing feature (`RefToMessageId`). Each message of an MEP Access Point refers to a previous message of the same Access Point, unless it is the first one to occur. +
Messages are associated with a label (e.g. `request`, `reply`) that precisely identifies their direction between the parties involved´ and their role in the choreography.

//
2+h|Bindings a|- a|Container.

//
2+h|Binding a|- a|The previous definition of ebMS MEP is quite abstract and ignores any binding consideration to the transport protocol. This is intentional, so that application level MEPs can be mapped to ebMS MEPs independently from the transport protocol to be used.

//
2+h|Roles a|- a|Container

//
2+h|Role a|Maps to `PMode.Initiator.Role` or `PMode.Responder.Role` depending on where this is used. In ebMS3 message this defines the content of the following element:

 * For Initiator: `Messaging/UserMessage/PartyInfo/From/Role`
 * For Responder: `Messaging/UserMessage/PartyInfo/To/Role`
 a| The required role element occurs once, and identifies the authorized
role (`fromAuthorizedRole` or `toAuthorizedRole`) of the Party sending the message (when present as a child of the `From` element), or receiving the message (when present as a child of the `To` element). The
value of the role element is a non-empty string, with a default value of `http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultRole`. +
Other possible values are subject to partner agreement.
//

2+h|PayloadProfiles
a|
a|*PayloadProfiles* Contains a list of `Payload` and `payloadProfile` elements.

2+h|PayloadProfile
a|maps to  +
`PMode[1].BusinessInfo.PayloadProfiles.PayloadProfile` more information in
<<payload_profile, Payload profile validation>>.
a|A payload profile consists of the following properties:

* `name` (required) - the part identifier.
* `maxSize` (required)-  the maximum size in kilobytes(kB) of the sum of all payloads.
* List of `attachment` (optional) - with a unique name referring to a specific `payload`.

2+h|Payload
a| maps to `PMode[1].BusinessInfo.PayloadProfiles.PayloadProfile[]`
a| This parameter allows specifying some constraint or profile on the payload.
It specifies a list of payload parts. +
A payload part is a data structure that consists of following properties:

1. `name` (required) - the part identifier, and can be used
as an index in the `PayloadProfile` notation.
2. `cid` or Content-ID (required) - is required to match the payload cid in the AS4 message +
(for e.g.: `href="cid:message">`).
3. `mimeType` (optional) - MIME data type (for e.g.: `text/xml`, `application/pdf`, etc.); +
matches the `MimeType` PartProperty in the AS4 message.
4. `inBody` This boolean indicates whether (if `true`) the payload is in body  (optional)
5. `schemaFile` - (not implemented).
6. `maxSize` (optional) - maximum size in kilobytes (kB).
7. `required`  (required) - Boolean string indicating whether the part is _expected_ or _optional_, within the User message. The message payload(s) must match this profile.

//
2+h|ErrorHandling a|- a|Container.

//
2+h|ErrorAsResponse
a| maps to `PMode[1].ErrorHandling.Report.AsResponse`

a|This Boolean parameter indicates (if `true`) that errors generated from
receiving a message in error are sent over the back-channel of the
underlying protocol associated with the message in error.
If `false`, such errors are not sent over the back-channel.

//
2+h|ProcessErrorNotifyProducer
a| maps to +
`PMode[1].ErrorHandling.Report.ProcessErrorNotifyProducer`
a|This Boolean parameter indicates whether (if `true`) the Producer
(`application/party`) of a User Message matching this PMode should be
notified when an error occurs in the Sending MSH, during processing of
the _User Message to be sent_.

//
2+h|ProcessErrorNotifyConsumer
a| maps to +
`PMode[1].ErrorHandling.Report.ProcessErrorNotifyProducer`
a|This Boolean parameter indicates whether (if _true_) the Consumer
(`application/party`) of a User Message matching this PMode should be
notified when an error occurs in the Receiving MSH, during processing of
the _received User message_.

//
2+h|DeliveryFailureNotifyProducer
a|maps to `PMode[1].ErrorHandling.Report.DeliveryFailuresNotifyProducer`
a|When sending a message with this reliability requirement (_Submit_ invocation),
one of the two following outcomes shall occur: +

* The Receiving MSH successfully delivers (`Deliver` invocation) the message to the Consumer.
* The Sending MSH notifies (`Notify` invocation) the Producer of a delivery failure.

//
2+h|Legs a|- a|Container.

//
2+h|Leg a|- a|As messages in the same MEP may be subject to different requirements - e.g. the reliability, security and error reporting of a response may not be the same as for a request – the PMode will be
divided into `legs`. Each user message label in an ebMS MEP is associated with a PMode leg. Each PMode leg has a full set of parameters for the six categories above (except for `General Parameters`), even if in many cases parameters can have the same value across all MEP legs. +
Signal messages that implement transport channel bindings (such as PullRequest) are also controlled by the same categories of parameters, except for `BusinessInfo group`.

//
2+h|Processes a|- a|Container which defines the different Processes.
//
2+h|Process a|- a|In `Process` everything is plugged together.
|===

[[pmode_uploadnew]]
==== Uploading New Configuration

*Upload the PMode file*

To update the PMode configuration and/or Truststore:

1. Connect to the Administration Console using the administrator's credentials.
By default, credentials are:

* User: `admin`;
* Password: to obtain the password, look for the phrase “Default password for user admin is” in the logs of http://localhost:8080/domibus/home[http://_localhost_:_8080_/domibus].
+
[NOTE]
====
* In a cluster environment, the PMode configuration is replicated automatically on all nodes.
* It is highly recommended to change the passwords for default users. See <<admintools, Administration Console>>.
====

:imagesdir: content/_common/images/pmode/

IMPORTANT: Duplicate parameters/entities are not allowed in PMode. XSD validation is used to find the duplicate entities. +

image:pmode1.png[width=904,height=573]

1. Click on the *PMode menu*:
+
image:pmode2.png[width=821,height=718]

2. Press the *Upload* button:
+
image:pmode3.png[width=904,height=785]

3. Press the *Choose File* button, and navigate to the PMode file, select
it and click on the *Open* button (or equivalent) in the standard dialog
box:
+
image:pmode4.png[image,width=902,height=784]

4. Once the file has been selected, click *OK* to upload the PMode XML file:
+
image:pmode5.png[image,width=904,height=741]

NOTE: Every time a PMode is updated, the truststore is also reloaded.

===== Upload the Truststore

To upload the trustore:

1. Select the *Truststore* menu and press the *Upload* button:
+
image:pmode6.png[width=904,height=573]

2. Navigate to the Truststore and select it by clicking on the *Open*
button (or equivalent) of the standard file open dialog:
+
image:pmode7.png[width=904,height=607]

3. Once the file has been selected, enter the keystore password and click
on the *OK* button to activate the new *truststore jks file:*
+
image:pmode8.png[width=904,height=686]

==== Message properties validation

While exchanging AS4 messages using PMode configuration, a user could
define Message Properties as in the example below:

[source, xml]
----
<ns:UserMessage>
….
<ns:MessageProperties>
<ns:Property
    name="originalSender">urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1</ns:Property>
<ns:Property
    name="finalRecipient">urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4</ns:Property>
</ns:MessageProperties>
…

</ns:UserMessage>
----


Domibus has a limitation of 1024 characters for the value of a Message
Property. If this value is exceeded, an EbMS3Exception is thrown on both
sending (C2) and receiving (C3) side and the message is not
submitted/accepted.

[source, xml]
----
<properties>
    <property name="originalSenderProperty"
        key="originalSender"
        datatype="string"
        required="true"/>
    <property name="finalRecipientProperty"
        key="finalRecipient"
        datatype="string"
        required="true"/>
    <propertySet name="eDeliveryPropertySet">
        <propertyRef property="finalRecipientProperty"/>
        <propertyRef property="originalSenderProperty"/>
    </propertySet>
</properties>
----

[[payload_profile]]
==== Payload Profile

Pmode allows a fine control of the payloads using Payload profiles.

[source, XML]
----
<payloadProfiles>
    <payload    name="businessContentPayload"
                cid="cid:message"
                required="true"
                mimeType="text/xml"/>
    <payload    name="businessContentAttachment"
                cid="cid:attachment"
                required="false"
                maxSize="2000"
                mimeType="application/octet-stream"/>

    <payloadProfile name="MessageProfile" maxSize="2147483647">
        <attachment name="businessContentPayload"/>
        <attachment name="businessContentAttachment"/>
    </payloadProfile>
</payloadProfiles>
----

Each `leg` can be associated with a separate `payloadProfile`.
The profile's `maxSize`(Kb), defines the maximum allowed for the sum total of all the payloads' lengths (uncompressed/unencrypted).

//Continue review here
Payload profiles can include a payload description enforcing the existence of a payload, by setting `required=true`, as well as, its max size, using `maxSize` (kB).

The selection of which payload description (`PayloadProfiles.payload`)

is affected to which payload is done by

exact match of `cid` and `mimeType`.


===== Using Regex Values

The `payload` attributes, `cid` and `mimetype` allow regex values with a few restrictions:

* Only one payload description with regex is allowed per `payloadProfile`. With,
** either both `cid` and `mimeType` holding regex values or,
** only one of them holding a regex value.
,while the other holds an exact match.

* The payload description is used only when a payload does not match _any_ other payload description.
+

.Example
[source, XML]
----
<payloadProfiles>
    <payload    name="businessContentPayload"
                cid="cid:message"
                required="true"
                mimeType="text/xml"/>
    <payload    name="businessContentAttachment"
                cid="cid:attachment"
                required="false"
                mimeType="application/octet-stream"/>
    <payload    name="other"
                cid="regex((cid):([A-Za-z0-9._-])*)"
                required="false"
                mimeType="regex(.*)"/>
    <payloadProfile
            name="MessageProfileWithRegex"
            maxSize="2147483647">
        <attachment name="businessContentPayload"/>
        <attachment name="businessContentAttachment"/>
        <attachment name="other"/>
    </payloadProfile>
</payloadProfiles>
----
_Where we accept:_
** any additional payloads with `cid` starting with 'cid:'
** any `mimeType`.


===== Allowing All Payloads

To allow all payloads, `payloadProfile` does not hold any payload description elements, only the `payloadProfile.maxSize`, which enforces the sum of all payloads needs to be lower than the value established in `maxSize`.

There is no restriction on the number of payloads or their composition.
//There is none because users do not define it or this something
// that happens by default when you do not use payload descriptions?
//I'm not sure why we are adding this line.

.Example
[source, XML]
----
    <payloadProfile
         name="MessageProfileAllWithTotalSizeLimit"
         maxSize="2147483647"/>
----
