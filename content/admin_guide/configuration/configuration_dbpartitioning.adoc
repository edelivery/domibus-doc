:imagesdir: ../images/scope8/

== Database Partitioning

Partitioning allows tables, indexes, and index-organized tables to be
*subdivided into smaller pieces*, enabling these database objects to be
managed and accessed at a finer level of granularity.

Domibus may be configured to use partitions on Oracle database. It uses
partitions by range for the main table and partitions by reference for
the other tables.

Domibus partitions are created based on the primary key's format
and with granularity of 1 hour. +

Used format is TSID.

* The number representing the primary key contains a timestamp part and a unique numeric part.
* The timestamp part is incremented by 15099494400000 after each our.

=== Partitioning a new database

1. Open a command line session, log in and execute the following commands:
+
[source, roomsql]
----
 sqlplus sys as sysdba
----
+
Password should be the one assigned during the Oracle installation.

2. Once logged in Oracle,
+
[source, roomsql]
----
CONNECT <edelivery_user> <1>
SHOW USER; <2>
@oracle-x.y.z-partitioning.ddl <3>

EXIT
----
+
_Where:_
+
<1> `<edelivery_user>` stands for the actual user's username.
<2> Should return: <edelivery_user>.
<3> and DDL/SQL scripts must be run with the @ sign from the location of the scripts.

The partitioning SQL script when ran, creates:

* `1-hour` partitions for 7 days in advance;
* an oracle job `GENERATE_PARTITIONS_JOB` that runs once every day. +

The `GENERATE_PARTITIONS_JOB` job is responsible to create new partitions from the 8^th^ day onwards to ensure continuity. This job must be closely monitored to verify if partitions are being created successfully.

NOTE: Partitioning is not yet implemented for MySql.

=== Partitioning Populated DBs or Upgrading Partitioned DBs

See <<domibus_upgrade, Upgrading Domibus>> for instructions per migration path.

[[dataretention_partitions]]
=== Data retention with partitions

With partitions, a new *retention mechanism* is in place for Domibus. It
is possible to configure Domibus to delete messages by dropping an
entire partition, once all messages on a specific partition have
expired.

On `conf/domibus/domibus.properties`, following property is set:

[source, properties]
----
domibus.retentionWorker.deletion.strategy= PARTITIONS
----

The retention mechanism is defined by the retention values configured in
the PMode. It computes the maximum retention period for all messages
(received, downloaded, sent or failed) and only verifies partitions that
are beyond this maximum value. This increases the chances that each
partition is only verified once before being dropped.

Once all messages on a partition have expired, the partition is dropped (all
messages are deleted at once).

*There is a direct dependency between the archiving mechanism and the
retention mechanism*. When archiving is enabled, the retention mechanism does not
delete messages unless they were previously archived.
For each partition, the retention mechanism checks if all messages are expired _and_
archived before dropping the partition.

If the Retention value:

* for a status is `-1` _and_ if there are no messages in that status on that partition, _then_ we drop that partition.

* is set to `-1`, _or_ no custom value is set: then database partitioning is not being taken full advantage.
+
NOTE: Set a value or use a different retention strategy to prevent messages from being kept forever.

For more about the default values of the retention policy's properties.
such as, the MPC configuration below that keeps downloaded messages for one hour, and undownloaded messages for 1 day, see Data Retention Policy.

The properties:

* `retention_sent_success`, for the `ACKNOWLEDGED` messages,
* `retention_sent_failure`, for the `SENT_FAILURE` messages and
* `retention_sent`, for the `ACKNOWLEDGED` and `SENT_FAILURE` messages

are optional properties.

//"It will keep", it what? confusing paragraph
It will keep the sent messages for 10 days. Users can choose to define the property as per their requirements for which type of messages they want to delete as part
of retention.

[source, xml]
----
<mpcs>
	<mpc name="defaultMpc"
	qualifiedName="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC"
	enabled="true"
	default="true"
	retention_downloaded="60"
	retention_undownloaded="1440"
               retention_sent_success="14400"
               retention_sent_failure="14400"
	retention_sent="14400" />
</mpcs>
----

[[partitions_alerts]]
=== Partitions alerts

//XREF
Following the logic described in Data retention with partitions section, all messages on a partition that is verified for expiration
should already be in the final state. If some messages are not in the
final state, an alert is triggered. The frequency of the alert may be
configured in `conf/domibus/domibus.properties` and by default is 1 (one
alert per day).

.Alert management: Partitions
[source, properties]
----
#Frequency in days between alerts
domibus.alert.partition.expiration.frequency_days=1
----
