:imagesdir: content/_common/images/eulogin/

== EU Login Integration

=== Description

Domibus is configured by default to use its own database for user
authentication and authorization, as seen in previous chapters.

But Domibus could also be configured and installed to use EU Login for
user authentication and authorization (even if this is not provided by
default by EU Login).

[NOTE]
.EU Login
====
EU Login is the European Commission's central user authentication service.
It allows authorised users to access a wide range of Commission resources, including websites, applications and  services, using a single sign on based on (EC) email address, password,
and if required, additional authenticating factors.

Click https://webgate.ec.europa.eu/cas/about.html?loginRequestId=ECAS_LR-3450166-N63zgmzcF3nwMyelJxzy8N4GQVFKtRta1zlE2UfXH9y6yjun8Wml4rogpNFzcIRrGxc3dAitaYiFtyDyTuXIn3F-PHslUMVSXYCySzcFlWvYJq-atOlLzj6PtzdbCeFzlGad4jt5zzRIwPJzbUQrQ6ZjOiba[here]
for more information on EU Login.

More details can also be found on internal Confluence page by clicking the following link:
https://webgate.ec.europa.eu/CITnet/confluence/pages/viewpage.action?pageId=24641907
====

Domibus with EU Login integration is available only for Weblogic server.

=== Installation and Configuration

==== Installation

For installation of Domibus with EU Login, please follow the steps
below:

1. Create DB schemas as per previous chapters for a single tenancy or
Multitenancy installation
2. Download domibus-msh-distribution-xyz-weblogic-ecas-configuration.zip and
domibus-msh-distribution-xyz-weblogic-ecas-war.zip
3. Install and configure Domibus war and configuration files into
Weblogic server – follow Weblogic guidelines as per previous chapters
4. Check that the `WebLog-sql-scripts.zipic` server has the latest compatible
ECASIdentityAsserter installed:

* Go to the Weblogic Server console ->
* Security Realms -> myrealm -> Providers:
+
image:image14.png[width=623,height=264]

5. Configure ecas-config-domibus.xml file and install it in the classpath
of Weblogic server.
+
An example of `ecas-config-domibus.xml` file is as shown below:
+
[source, xml]
----
<client-config
    xmlns="https://www.cc.cec/cas/schemas/client-config/ecas/1.8"
    xmlns:cas="https://www.cc.cec/cas/schemas/client-config/cas/2.0">
    <ecasBaseUrl>https://ecasa.cc.cec.eu.int:7002</ecasBaseUrl>
    <groups>
        <group>*</group>
    </groups>
    <acceptStrengths>
        <strength>STRONG</strength>
        <strength>STRONG_SMS</strength>
        <strength>CLIENT_CERT</strength>
    </acceptStrengths>
    <assuranceLevel>LOW</assuranceLevel>
    <!-- renew is false only for local in order to speedup the development-->
    <cas:renew>true</cas:renew>
    <requestingUserDetails>true</requestingUserDetails>
</client-config>
----

For more details about steps d. and e., please refer to EU Login
documentation in the Confluence pages provided above.
//steps D and E?

==== Configuration

When a user is authenticated against EU Login he or she has specific
LDAP groups associated with him/her. These groups will be used for
Domibus to map:

* User roles: AP_ADMIN, ADMIN and USER
* Default domain

The mapping of these groups is performed in `domibus.properties` which
needs to be changed accordingly. Look for the section related to EU
Login mappings and update it:


[source, properties]
----
domibus.security.ext.auth.provider.group.prefix=DIGIT_DOM
----

This is the prefix of EU Login LDAP groups that Domibus will take into
account.

[source, properties]
----
domibus.security.ext.auth.provider.user.role.mappings=DIGIT_DOMRUSR=ROLE_USER;DIGIT_DOMRADM=ROLE_ADMIN;DIGIT_DOMRSADM=ROLE_AP_ADMIN;
----

This property will map each EU Login LDAP group to a corresponding Domibus user role.
If one user has more than one LDAP group/role associated, the role with the broader rights will be chosen.

[source, properties]
----
domibus.security.ext.auth.provider.domain.mappings=DIGIT_DOMDDOMN1=domain1;
----

This property will map an EU Login LDAP group to a Domibus domain: it is
useful in a Multitenancy installation, as in single tenancy all users
are mapped to Default domain.

If the current user has no roles/LDAP groups or domain associated,
he/she could still authenticate but he or she will not have the
privileges to use the Domibus console.


.EU Login mappings
[source, properties]
----
# All EU Login groups used by Domibus should have this prefix
domibus.security.ext.auth.provider.group.prefix=DIGIT_DOM

# Pairs of strings separated by semicolons to map Domibus user roles
# to EU Login LDAP groups, the format is
LDAP_GROUP_USER=ROLE_USER;LDAP_GROUP_ADMIN=ROLE_ADMIN;LDAP_GROUP_AP_ADMIN=ROLE_AP_ADMIN;

# Last semicolon is mandatory
domibus.security.ext.auth.provider.user.role.mappings=DIGIT_DOMRUSR=ROLE_USER;DIGIT_DOMRADM=ROLE_ADMIN;DIGIT_DOMRSADM=ROLE_AP_ADMIN;

# Pairs of strings separated by semicolons to map Domibus domain codes
# to EU Login LDAP groups the format is LDAP_GROUP_DOMAIN1=domain1;LDAP_GROUP_DOMAIN2=domain2;
# last semicolon is mandatory
domibus.security.ext.auth.provider.domain.mappings=DIGIT_DOMDDOMN1=domain1;
----

=== Domibus UI changes

Access to the Domibus user interface is performed using EU Login.

This means when users access Domibus via the http://server:port/domibus URL,
they are redirected to the EU Login page where they need to fill in the username and password.
(QUESTION they EU Login crßedentials)
After successfully entering their credentials, users are provided access to the Domibus application.

Non-super-administrator users that can manage multiple domains will see a domain dropdown allowing them to switch between all available domains with that role.

The user's username appears on the right-hand corner on the Domibus Admin console but some options might be disabled or unavailable (greyed out), such as:

* *Change Password* (from top right menu): as the password change is managed by the EU Login.
* *Users* (from left menu): adding or editing existing users will not be possible.
