[[installation_prereqs]]
=== Pre-requisites

Below you can find a list of requirements for your Dominus installation.

Select and the relevant combination of software required to deploy Domibus on the target system(s). +
See also ☛ <<domibus_downloads>>.

[cols="60%,40%"]
|===

2+h| Java
a| Java 8 features, compile with Oracle JDK 8.

Domibus supported webservers were tested to run correctly against the following Java Development Kits:

a|
* *Tomcat, WildFly and WebLogic*: _Oracle JRE version 8u291+_. +
Download it http://www.oracle.com/technetwork/java/javase/downloads/index.html[here].

* *Tomcat and WildFly*: _OpenJDK 11.0.11_. +
Tested with AdoptOpenJDK version 11.0.9.1+1. Download it https://openjdk.java.net/projects/jdk/11/[here]

2+h|Webservers

2+a|*Single Server* _General scenario_

a|In this scenario you need install one of the following:

NOTE: General as in other than the *Single Server* _Pre-Configured_ scenarios when applicable for a supported server. +
*See also:* +
<<tomcat_predef_single>> +
<<wildfly_preconfigsingle>>

a|
* *WebLogic* _12.2.1.4_, or higher
* *WildFly* _26.1.x Final_ , or higher
* *Tomcat* _9.x_, or higher

2+a|*Clustered scenario*

a| If deploying Domibus across multiple server instances, then it's required to install
one the following supported versions:

a|
* *WebLogic* _12.2.1.4_
* *WildFly* _26.1.x Final_
* *Apache* _Tomcat 9.x_


2+h| Databases

a|Supported Database Management Systems:

a|
* MySQL: *8.0.13* or higher.
* Oracle: *12c R2 and Oracle 19c* or higher.
|===

[caption=, title=Third-party software documentation]
====
For additional information and installation details regarding the third-party software listed above, refer to its manufacturer's documentation.
====

.Notes on support
****
.Security
Users installing any of the Domibus packages labelled as _Full Distribution_ have the responsibility to update application servers to their latest version after installation to ensure their system’s security.

.Browser Support
* The Domibus application is supported in all modern web browsers except Internet Explorer.

.Versions Tested
* We tested the specific versions mentioned above. Higher, later versions may also work.
****



