==== Oracle

To configure your Oracle DB:

1. Unzip `domibus-msh-distribution-{latestvs}-sql-scripts.zip` in
`<edelivery_path>/sql-scripts`.

2. Open a command prompt and navigate to the `<edelivery_path>/sql-scripts` directory.

Open a command line session, log in and execute the following commands:

[source, roomsql]
----

sqlplus sys as sysdba
/* Password should be the one assigned
during the Oracle installation */
----

Once you're logged in in Oracle:

[source,roomsql]
----
CREATE USER <edelivery_user> IDENTIFIED BY
<edelivery_password>
DEFAULT TABLESPACE <tablespace>
QUOTA UNLIMITED ON <tablespace>;
GRANT CREATE SESSION TO <edelivery_user>;
GRANT CREATE TABLE TO <edelivery_user>;
GRANT CREATE VIEW TO <edelivery_user>;
GRANT CREATE SEQUENCE TO <edelivery_user>;
GRANT CREATE PROCEDURE TO <edelivery_user>;
GRANT CREATE JOB TO <edelivery_user>;
GRANT EXECUTE ON DBMS_XA TO <edelivery_user>;
GRANT EXECUTE ON DBMS_LOCK TO <edelivery_user>;
GRANT SELECT ON PENDING_TRANS$ TO <edelivery_user>;
GRANT SELECT ON DBA_2PC_PENDING TO <edelivery_user>;
GRANT SELECT ON DBA_PENDING_TRANSACTIONS TO <edelivery_user>;
CONNECT <edelivery_user>
SHOW USER; (should return: edelivery_user)
@oracle-x.y.z.ddl
@oracle-{latestvs}-data.ddl

EXIT
----

1. Replace `<edelivery_user>` and `<edelivery_password>` with corresponding values.

2. <tablespace> is created and assigned by your DBA; for local/test
installations just replace it with user’s tablespace. The quota could be
limited to a specific size.

3. DDL/SQL scripts must be run with the @ sign from the location of the
scripts.

Please find below some information regarding the Oracle configuration:
//(Output? Results?)

[source, roomsql]
----
The Oracle JDBC driver is configured now to use the UTC server timezone by default when persisting and reading date time values. For future date time values – e.g. next attempts for the retry mechanisms – we also save the timezone offset when persisting in order to be able to recreate the correct instant when reading back later on, in the event the timezone offset will have changed while waiting for the future event to occur.
----

