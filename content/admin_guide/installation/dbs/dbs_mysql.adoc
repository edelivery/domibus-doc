==== MySQL

include::../../../_common/includes/note_installpath.adoc[]

To configure your MySQL database:

1. Download `domibus-msh-distribution-{latestvs}-sql-scripts.zip`. See <<domibus_downloads, Download resources>>.
2. Extract the downloaded archive into `<edelivery_path>/sql-scripts`.
3. From your command-line interface (CLI), explore the `<edelivery_path>/sql-scripts` path where you placed the scripts.
4. From the that location, execute the MySQL commands as instructed bellow.

[discrete]
===== Create the Domibus DB Schema
To create the `domibus_schema` and the `edelivery_user` user with all the privileges for the schema, execute:

[source,roomsql]
----
mysql -h localhost -u root_user--password=root_password -e "drop schema if exists domibus_schema
create schema domibus_schema;
alter database domibus_schema charset= utf8mb4 collate= utf8mb4_bin;
create user edelivery_user@localhost identified by 'edelivery_password';
grant all on domibus_schema.* to edelivery_user@localhost;"
----

===== Create Domibus Schema's Required Objects

To create the required objects in the Domibus schema, execute:

[source,bash]
----
mysql -h localhost -u root_user --password=root_password -e "grant
xa_recover_admin on *.* to edelivery_user@localhost;"

mysql -h localhost -u edelivery_user --password=edelivery_password domibus_schema < mysql-x.y.z.ddl
----

[discrete]
===== Populate the tables

[source,bash]
----
mysql -h localhost -u root_user --password=root_password domibus_schema < mysqlinnoDb-{latestvs}-data.ddl
mysql -h localhost -u edelivery_user --password=edelivery_password domibus_schema < mysql-x.y.z-data.ddl
----

[discrete]
===== Additional Settings

Here are some additional server settings we recommend setting for your MySQL DB.

[discrete]
===== Storing Messages Payloads in a DB

.Storing payload messages with size over 30 Mb in a database
Domibus can store the messages in the database _temporarily_.
So it is advised to increase the maximum allowed size for packets.

To increase max size for packets, edit `my.ini` file in Windows or, in Linux, the `my.cnf` file, and update the following default properties as indicated below:

1. Edit the `max_allowed_packet` property:
+
[source, bash]
----
# Maximum size of packets or any generated or intermediate string,
# or any parameter sent by the mysql_stmt_send_long_data() C API function.

max_allowed_packet=512M
----

2. Edit the `innodb_log_file_size` property:
+
[source, bash]
----
# Size of each log file in a log group. Set the combined size
# of log files to about 25%-100% of your buffer pool size to avoid
# unnecessary buffer pool flush activity on log file overwrite.
# Note that larger logfile sizes increase the time needed for the recovery process.

innodb_log_file_size=5120M
----

3. Restart MySQL service (Windows)

//(image missing)

[discrete]
===== Storing Messages Payloads in the FS

For storing Messages Payloads in the File System or other customer repositories as opposed to the Database, see <<domibus_properties, Domibus properties>> section.

[discrete]
===== Setting the database timezone to UTC

For MySQL 8 and Connector/J 8.0.x please

One way of setting the timezone is to modify the MySQL `my.ini` configuration file by adding the following property with the adjusted timezone to UTC.

[source, bash]
----
default-time-zone='+00:00'
----

The connector is now configured to use the UTC server timezone by default.
For future date time values – e.g. next attempts for the retry mechanisms) – we also save the timezone offset when persisting in order to be able to recreate the correct instant when reading back later on, in the event the timezone offset will have changed while waiting for the future event to occur.

NOTE: If you are using Windows, make sure to have the parent directory of
mysql.exe added to your PATH variable.

