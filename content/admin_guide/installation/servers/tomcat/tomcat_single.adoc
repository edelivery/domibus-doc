//imagesdir declared in parent file where include is used

[[tomcat_single_deployment]]
===== ☛ Single Server Deployment

For this step, you have the following resources available. See <<domibus_downloads>>.

* `domibus-msh-distribution-{latestvs}-tomcat-configuration.zip`
* `domibus-msh-distribution-{latestvs}-tomcat-war.zip`
* `domibus-msh-distribution-{latestvs}-sql-scripts.zip`
* `domibus-msh-distribution-{latestvs}-tomcat-configuration.zip`
* `domibus-msh-distribution-{latestvs}-default-ws-plugin.zip`
* `domibus-msh-distribution-{latestvs}-default-jms-plugin.zip (optional)`
* `domibus-msh-distribution-{latestvs}-default-fs-plugin.zip (optional)`
* `domibus-msh-distribution-{latestvs}-tomcat-war.zip`
* `domibus-msh-distribution-{latestvs}-sample-configuration-and-testing.zip`
* `domibus-MSH-tomcat-{latestvs}.war`
* `Mysql-connector-java-x.y.z driver` +
(eg.: `mysql-connector-java-8.0.23.jar`, see http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/profiles/AS4-profile/v1.0/[OASIS AS4 Profile])
+
We assume that an Apache Tomcat 9.x is already installed and the installation location is now considered as your `<edelivery_path>`/*.*

1. Download and unzip the artefact `domibus-msh-distribution-{latestvs}-tomcat-configuration.zip` into the directory <edelivery_path>/conf/domibus.
2. Configure the MySQL or Oracle datasource as indicated in Pre-Configured Single Server Deployment.
3. Configure your Keystore based on Certificates.
4. Execute _step 4_ from Pre-Configured Single Server Deployment.
5. If not already present, create a folder and name it `temp` under <edelivery_path>/conf/Domibus.
6. Rename domibus-MSH-{latestvs}-tomcat.war to `domibus.war` and deploy it to <edelivery_path>/webapps.
+
image::image21.png[image,width=315,height=63]

7. Copy Plugins subfolders to the conf/Domibus/plugins folder
8. Add the `conf/domibus` path (to `catalina.sh` or `setenv.sh`). Add the following highlighted lines:
+
[source,properties]
----

JAVA_OPTS="$JAVA_OPTS -Djava.protocol.handler.pkgs=org.apache.catalina.webresources"
export JAVA_OPTS="$JAVA_OPTS -Xms128m -Xmx1024m"
export=JAVA_OPTS="$JAVA_OPTS -domibus.config.location=$CATALINA_HOME/conf/domibus"
#Check for the deprecated LOGGING_CONFIG

----
9. Copy the Mysql connector (e.g.: mysql-connector-java-8.0.23.jar) to the lib folder.
10. From `domibus-msh-distribution-{latestvs}-sample-configuration-and-testing.zip`, copy
the keystores folder to `…/conf/domibus`.
11. Rename the `domibus-MSH-tomcat-{latestvs}.war` to Domibus.war and copy it
to webapps
12. Launch the Domibus application:
+
** For Windows:
+
[source, bash]
----
cd <edelivery_path>\bin\
startup.bat
----
+
** For Linux:
+
[source, bash]
----
cd <edelivery_path>/bin/
chmod +x .sh
./startup.sh
----

 13. Display the Domibus home page on your browser: http://localhost:8080/domibus-wildfly/home[http://localhost:8080/domibus]
+
If you can access the page below, then you have successfully completed the installation.
+

image::image18.png[width=604,height=309]

include::../../../../_common/includes/note_userspwd.adoc[]
