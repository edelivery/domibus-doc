=== References

//In admin guide docx
[width="100%",cols="10%,40%,50%",options="header"]
|===
|# |*Document* |*Contents outline*
a|REF1
|https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/eDelivery+AS4+conformant+solutions[eDelivery
AS4 Profile] |The eDelivery AS4 Profile is a profile of the http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/core/os/ebms_core-3.0-spec-os.pdf[ebMS3]and http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/profiles/AS4-profile/v1.0/AS4-profile-v1.0.pdf[AS4 OASIS
Standards. It has provisions for use in four-corner topologies, but it
can also be used in point-to-point exchanges.

a|REF2
|http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/profiles/AS4-profile/v1.0/[OASIS
AS4 Profile] |AS4 Profile of ebMS 3.0 Version 1.0. OASIS Standard, 23 January 2013.

a|REF3
|http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/core/os/[ebMS3 Core]
|OASIS ebXML Messaging Services Version 3.0: Part 1, Core Features.
OASIS Standard. 1 October 2007.

a|REF4
|https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus[Domibus
plugin cookbook] |Technical manual on Domibus plugin development.
//changed to cookbook xref in search results

a|REF5
|http://cxf.apache.org/[Apache CXF] |Apache CXF is an open source
services framework. These services can speak a variety of protocols such
as SOAP, XML/HTTP, RESTful HTTP, or CORBA and work over a variety of
transports such as HTTP, JMS or JBI.

a|REF6
|https://ws.apache.org/wss4j[Apache WSS4J] |The Apache WSS4J™ project
provides a Java implementation of the primary security standards for Web
Services, namely the OASIS Web Services Security (WS-Security)
specifications from the
http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=wss[OASIS Web
Services Security TC]

a|REF7
|https://www.w3.org/TR/ws-policy/[WS-Policy Specification] |The Web
Services Framework provides a general-purpose model and corresponding
syntax to describe the policies of entities in a Web services-based
system.

a|Contains
|http://projects.spring.io/spring-security/[Spring Security] |Spring
Security is a framework that focuses on providing both authentication
and authorization to Java applications. It can easily be extended to
meet custom requirements.

a|REF9
|Bcrypt | Provos, Niels; Mazières, David; Talan Jason Sutton 2012 (1999).
http://www.usenix.org/events/usenix99/provos/provos_html/node1.html["A
Future-Adaptable Password Scheme"]. Proceedings of 1999 USENIX Annual
Technical Conference: 81–92.

a|REF10
|https://www.jcp.org/en/jsr/detail?id=914[JMS] |The Java Message Service
(JMS) API is a Java Message Oriented Middleware API for sending messages
between two or more clients.

a|
REF11
|http://www.e-codex.eu/home.html[e-CODEX] |The e-CODEX project improves
the cross-border access of citizens and businesses to legal means in
Europe and furthermore creates the interoperability between legal
authorities within the EU.

a|REF12
|https://www.jcp.org/en/jsr/detail?id=315[Java Servlet 3.0] |A Java
servlet is a Java program that extends the capabilities of a server.
Although servlets can respond to any types of requests, they most
commonly implement applications hosted on Web servers. Such Web servlets
are the Java counterpart to other dynamic Web content technologies such
as PHP and ASP.NET.

a| REF13
|http://www.eclipse.org/Xtext/[Xtext] |Xtext is a framework for
development of programming languages and domain-specific languages.

// Missing 14th?
// https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Guidance+on+Digital+Certificates

a|REF15
|http://www.w3.org/TR/soap/[SOAP] |Simple Object Access Protocol

a|REF16
|http://en.wikipedia.org/wiki/Chunked_transfer_encoding[HTTP Chunking]
|A mechanism by which data is broken up into a number of chunks when
sent over an HTTP connection.
|===
