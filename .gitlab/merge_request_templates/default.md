# 💬 Description
📝 **instruction:** EDIT the list above for a brief and clear description for what is added, removed, changed or updated in documentation.

# 🔢 Affected Versions
📝 **instruction:** REPLACE this line with EACH release (publish or unpublished) that ALSO needs to have these updates OR IF N/A, DELETE this line.

## ✅ Author's Check List
Before creating your MR, check:

| Check MR's           
|-----------------
|- **Branch Name** <br> _Rule_: Follows the pattern ^\bEDELIVERY-\b\b\d{5,}\b$  <br> _Example_: EDELIVERY-13177 
|- **Title** <br> Rule: Starts with \bEDELIVERY-\b\b\d{5,}\s <br> Example: EDELIVERY-13177 Updates S3 Configuration
|- **Commit Messages** <br> Rule: Start with the pattern[EDELIVERY-<issueNumber><space><Short Commit Message> <br> Example: [EDELIVERY-13477] Adds new auth properties re S3
|- **Description** is helpfull/edited.
|- All **affected versions** are indicated.

### Questions? 
See 📙 [Updating eDelivery Documentation](https://ec.europa.eu/digital-building-blocks/wikis/display/EDELINT/Updating+eDelivery+Tech+Documentation+-+Process+in+place).


/draft
/label ~Tech Review::WIP
/label ~NotToBeTested
/assign @nevesdo
/request_review @draguio @baciuco @rihtajo @tincuse @gautifr @maierga @breazio @durlaio @furcalu @azhikso @perpeio @venugar @ghouiah @bozmiha @monhaso @vedutvl


